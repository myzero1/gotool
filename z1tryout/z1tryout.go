package z1tryout

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"gitee.com/myzero1/gotool/z1err"
)

var TryOutDeadline = "2021-12-28 22:01:35"
var CheckUrlPre = `https://gitee.com/myzero1/tryout/raw/master/test/`

func SetTryout(setting ...string) {
	if len(setting) > 0 {
		CheckUrlPre = setting[0]
	}

	if len(setting) > 1 {
		TryOutDeadline = setting[1]
	}
}

func TryoutExpired() (retCanUse bool) {
	deadline1, err := time.ParseInLocation(`2006-01-02 15:04:05`, TryOutDeadline, time.Local)
	z1err.Check(err)
	if time.Now().Unix() <= deadline1.Unix() {
		return true
	}

	isVm, err := IsVirtualMachine()
	z1err.Check(err)
	if isVm {
		return false
	}

	cpuId, err := GetCPUID()
	z1err.Check(err)
	if cpuId == "" {
		return false
	}

	// https://gitee.com/myzero1/tryout/raw/master/test/BFEBFBFF000906EA
	url := fmt.Sprintf(`%s%s`, CheckUrlPre, cpuId)
	resp, err := http.Get(url)
	z1err.Check(err)

	if resp.StatusCode != 200 {
		return false
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	z1err.Check(err)
	bodystr := string(body)
	bodystr = strings.Trim(bodystr, ` `)
	deadline, err := time.ParseInLocation(`2006-01-02 15:04:05`, bodystr, time.Local)
	z1err.Check(err)

	remoteDate := resp.Header["Date"][0]
	remoteDate = strings.Split(remoteDate, `, `)[1]
	remoteTime, err := time.ParseInLocation(`02 Jan 2006 15:04:05 GMT`, remoteDate, time.Local)
	remoteTime = remoteTime.Add(8 * time.Hour)
	z1err.Check(err)

	if remoteTime.Unix() <= deadline.Unix() {
		return true
	}

	return
}

func IsVirtualMachine() (retIsVM bool, retErr error) {
	defer z1err.Handle(&retErr)

	// https://www.dazhuanlan.com/fangzhou422/topics/1681308
	model := ""
	var cmd *exec.Cmd

	if runtime.GOOS == "windows" {
		cmd = exec.Command("cmd", "/C", "wmic path Win32_ComputerSystem get Model")
	} else { // linux
		cmd = exec.Command("/bin/bash", "-c", "dmidecode | egrep -i 'system-product-name|product|domU'")
	}

	stdout, err := cmd.Output()
	z1err.Check(err)

	model = string(stdout)

	if strings.Contains(model, "VirtualBox") || strings.Contains(model, "Virtual Machine") || strings.Contains(model, "VMware Virtual Platform") ||
		strings.Contains(model, "KVM") || strings.Contains(model, "Bochs") || strings.Contains(model, "HVM domU") {
		retIsVM = true
	}

	return
}

//GetCPUID 获取cpuid
func GetCPUID() (retCpuid string, retErr error) {
	defer z1err.Handle(&retErr)

	// https://www.cnblogs.com/wuyaxiansheng/p/13576174.html
	cmd := exec.Command("wmic", "cpu", "get", "processorid")
	b, err := cmd.CombinedOutput()
	z1err.Check(err)

	cpuid := string(b)
	cpuid = cpuid[12 : len(cpuid)-2]
	cpuid = strings.ReplaceAll(cpuid, "\n", "")
	cpuid = strings.ReplaceAll(cpuid, "\r", "")
	cpuid = strings.ReplaceAll(cpuid, " ", "")

	retCpuid = cpuid

	return
}

//GetBaseBoardID 获取主板的id
func GetBaseBoardID() (retBoardID string, retErr error) {
	defer z1err.Handle(&retErr)

	cmd := exec.Command("wmic", "baseboard", "get", "serialnumber")
	b, err := cmd.CombinedOutput()
	z1err.Check(err)

	cpuid := string(b)
	cpuid = cpuid[12 : len(cpuid)-2]
	cpuid = strings.ReplaceAll(cpuid, "\n", "")
	retBoardID = cpuid

	return
}
