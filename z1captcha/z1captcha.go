package z1captcha

import (
	"image/color"

	"gitee.com/myzero1/gotool/z1struct"
	"github.com/mojocn/base64Captcha"
)

type Param struct {
	// Height png height in pixel.
	Height int `z1Default:"60"`
	// Width Captcha png width in pixel.
	Width int `z1Default:"180"`
	// DefaultLen Default number of digits in captcha solution.
	Length int `z1Default:"6"`
	// Captcha Timeout.
	Timeout int `z1Default:"300"`
	// Captcha's id length.
	IDLength int `z1Default:"10"`

	//NoiseCount text noise count.
	NoiseCount int `z1Default:"0"`
	//ShowLineOptions := OptionShowHollowLine | OptionShowSlimeLine | OptionShowSineLine .
	ShowLineOptions int `z1Default:"14"`
	//Source is a unicode which is the rand string from.
	Source string `z1Default:"1234567890abcdefghijklmnopqrstuvwxyz"`
	//Password used in no store for id
	Password string `z1Default:"myzero1"`
	//BgColor captcha image background color (optional)
	BgColor *color.RGBA
}

func NewZ1captcha(params ...Param) (captcha *base64Captcha.NoStoreCaptcha) {
	defaultParam := Param{BgColor: &color.RGBA{R: 255, G: 255, B: 255, A: 255}}
	z1struct.FillDefault(&defaultParam)
	var param Param
	if len(params) > 0 {
		param = params[0]
		z1struct.Load(&param, &defaultParam)
	} else {
		param = defaultParam
	}

	store := base64Captcha.NewNoStore(param.Password, param.Timeout, param.IDLength)
	driver := base64Captcha.NewDriverString(param.Height,
		param.Width,
		param.NoiseCount,
		param.ShowLineOptions,
		param.Length,
		param.Source,
		param.BgColor,
		[]string{})
	captcha = base64Captcha.NewNoStoreCaptcha(driver, store)

	return
}
