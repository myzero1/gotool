package z1db

import (
	"os"
	"path"

	"gitee.com/myzero1/gotool/z1db/gormplugin"
	"gitee.com/myzero1/gotool/z1err"
	"gitee.com/myzero1/gotool/z1mongo"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/cloudquery/sqlite" // no cgo
	// "gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var DB *DBs

// use addStatement
//
//	 db = db.WithContext(context.WithValue(
//		context.Background(),
//		`z1AddStatement`,
//		map[string]interface{}{
//			`status`: 735,
//		},
//
// ))
//
//	 use sql2gorm
//		sql2gorm: $(go env GOMODCACHE)/$(go mod graph | grep 'gitee.com/myzero1/gotool' | head -n 2 | tail -n 1 | sed 's/ .*//')/z1db/scripts/z1sql2gorm.sh
//
//		github.com/cloudquery/sqlite   // no cgo,go get github.com/cloudquery/sqlite@v1.0.1
//		gorm.io/driver/sqlite
//		gorm.io/driver/mysql
//		gorm.io/driver/postgres
func NewDB(mongoCnf z1mongo.MongoConfig, tomongo gormplugin.ToMongo, addStatement gormplugin.AddStatement, gormConfig gorm.Config, dialectors ...gorm.Dialector) (err error) {
	// _ = gorm.Z1Model{} // err: undefined: gorm.Z1Model, run: $(go env GOMODCACHE)/$(go mod graph | grep 'gitee.com/myzero1/gotool' | head -n 2 | tail -n 1 | sed 's/ .*//')/z1db/scripts/z1db-install.sh

	// mongoCnf = z1mongo.MongoConfig{
	// 	Uri:          `mongodb://localhost:27017`,
	// 	DBName:       `z1mongo`,
	// 	Timeout:      time.Duration(time.Second * 5),
	// 	ClientMaxNum: 200,
	// }

	empty := z1mongo.MongoConfig{}
	if mongoCnf != empty {
		z1mongo.NewZ1mongo(mongoCnf)
	}

	var dialector gorm.Dialector
	if len(dialectors) > 0 {
		dialector = dialectors[0]
	} else {
		// _, filename, _, ok := runtime.Caller(0)
		// if !ok {
		// 	err = errors.New("No caller information")
		// 	return
		// }
		// dir := path.Dir(path.Dir(filename))
		// dialector = sqlite.Open(dir + `/test/z1db/sqlite.db`)

		sqliteFile := path.Join(os.TempDir(), `z1db_tmp_sqlite.db`)
		dialector = sqlite.Open(sqliteFile)

	}

	// dialector = sqlite.Open(`z1db/sqlite.db`)

	// dialector = mysql.New(mysql.Config{
	// 	DSN:                       "root:@tcp(127.0.0.1:3306)/z1gorm_test?charset=utf8&parseTime=True&loc=Local", // DSN data source name
	// 	DefaultStringSize:         256,                                                                           // string 类型字段的默认长度
	// 	DisableDatetimePrecision:  true,                                                                          // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
	// 	DontSupportRenameIndex:    true,                                                                          // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
	// 	DontSupportRenameColumn:   true,                                                                          // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
	// 	SkipInitializeWithVersion: false,
	// })

	if gormConfig.NamingStrategy == nil {
		gormConfig.NamingStrategy = schema.NamingStrategy{
			// TablePrefix:   "t_", // 表名前缀，`User` 的表名应该是 `t_users`
			SingularTable: true, // 使用单数表名，启用该选项，此时，`User` 的表名应该是 `t_user`
		}
	}

	db, err := gorm.Open(dialector, &gormConfig)
	z1err.Check(err)

	if mongoCnf != empty {
		toMongoZero := gormplugin.ToMongo{}
		if tomongo != toMongoZero {
			// z1err.Check(db.Use(&gormplugin.ToMongo{MongoAll: false, MongoTable: `,mongo_user,mongo_role,mongo_user_role,`}))
			// z1err.Check(db.Use(gormplugin.NewToMongo(false, `,mongo_user,mongo_role,mongo_user_role,`)))
			z1err.Check(db.Use(&tomongo))
		}
	}

	addStatementZero := gormplugin.AddStatement{}
	if addStatement != addStatementZero {
		// z1err.Check(db.Use(&gormplugin.ToMongo{MongoAll: false, MongoTable: `,mongo_user,mongo_role,mongo_user_role,`}))
		// z1err.Check(db.Use(gormplugin.NewToMongo(false, `,mongo_user,mongo_role,mongo_user_role,`)))
		z1err.Check(db.Use(&addStatement))
	}

	db2, err := gorm.Open(dialector, &gormConfig)
	z1err.Check(err)

	{
		// ModifyStatementWhereSettingDefault = ModifyStatementWhereSetting{
		// 	On:             true,
		// 	ExcludedTables: `,user,role,`,
		// 	FieldName:      `id`,
		// 	FieldValue:     123456,
		// }
	}

	// Callback
	{
		// db.Callback().Create().Before("*").Register("z1_Z1ToDryRun_Callback-Create", func(db *gorm.DB) {
		// 	Z1ToDryRun(db)
		// 	ModifyStatementCreate(db)
		// 	// log.Println(`-------------z1_Z1ToDryRun_Callback----Create------------`, db.Dialector.Explain(db.Statement.SQL.String(), db.Statement.Vars...))
		// })
		// db.Callback().Create().After("*").Register("z1_Z1ToMongo_Callback-Create", func(db *gorm.DB) {
		// 	// log.Println(`-------------z1_Z1ToMongo_Callback-------Create---------`, db.Dialector.Explain(db.Statement.SQL.String(), db.Statement.Vars...))
		// 	Z1ToMongo(db)
		// })

		// db.Callback().Delete().Before("*").Register("z1_Z1ToDryRun_Callback-Delete", func(db *gorm.DB) {
		// 	Z1ToDryRun(db)
		// 	ModifyStatementWhere(db)
		// 	// log.Println(`-------------z1_Z1ToDryRun_Callback----Delete------------`, db.Dialector.Explain(db.Statement.SQL.String(), db.Statement.Vars...))
		// })
		// db.Callback().Delete().After("*").Register("z1_Z1ToMongo_Callback-Delete", func(db *gorm.DB) {
		// 	// log.Println(`-------------z1_Z1ToMongo_Callback-------Delete---------`, db.Dialector.Explain(db.Statement.SQL.String(), db.Statement.Vars...))
		// 	Z1ToMongo(db)
		// })

		// db.Callback().Update().Before("*").Register("z1_Z1ToDryRun_Callback-Update", func(db *gorm.DB) {
		// 	Z1ToDryRun(db)
		// 	ModifyStatementWhere(db)
		// 	// log.Println(`-------------z1_Z1ToDryRun_Callback----Update------------`, db.Dialector.Explain(db.Statement.SQL.String(), db.Statement.Vars...))
		// })
		// db.Callback().Update().After("*").Register("z1_Z1ToMongo_Callback-Update", func(db *gorm.DB) {
		// 	// log.Println(`-------------z1_Z1ToMongo_Callback-------Update---------`, db.Dialector.Explain(db.Statement.SQL.String(), db.Statement.Vars...))
		// 	Z1ToMongo(db)
		// })

		// db.Callback().Query().Before("*").Register("z1_Z1ToDryRun_Callback-Query", func(db *gorm.DB) {
		// 	Z1ToDryRun(db)
		// 	ModifyStatementWhere(db)
		// 	// log.Println(`-------------z1_Z1ToDryRun_Callback----Query------------`, db.Dialector.Explain(db.Statement.SQL.String(), db.Statement.Vars...))
		// })
		// db.Callback().Query().After("*").Register("z1_Z1ToMongo_Callback-Query", func(db *gorm.DB) {
		// 	// log.Println(`-------------z1_Z1ToMongo_Callback-------Query---------`, db.Dialector.Explain(db.Statement.SQL.String(), db.Statement.Vars...))
		// 	Z1ToMongo(db)
		// })
	}

	DB = &DBs{
		Gorm:       db,
		GormOrigin: db2,
		Mongo:      z1mongo.DB,
	}

	// View the SQL that Gorm will execute
	{
		// // https://gorm.io/docs/sql_builder.html#DryRun-Mode
		// // https://gorm.io/docs/session.html#DryRun
		// txSql := db.Session(&gorm.Session{DryRun: true}).Model(&z1db.MongoUserRole{}).Joins(
		// 	"JOIN mongo_users ON mongo_user_roles.uid = mongo_users.id",
		// ).Joins(
		// 	"JOIN mongo_roles ON mongo_user_roles.rid = mongo_roles.id",
		// ).Select(
		// 	"mongo_users.username",
		// 	"mongo_roles.name",
		// ).Find(&joinMap)
		// log.Println(`-------View the SQL that Gorm will execute---------`, db.Dialector.Explain(
		// 	txSql.Statement.SQL.String(),
		// 	txSql.Statement.Vars...,
		// ))
	}

	return
}

type DBs struct {
	Gorm       *gorm.DB
	GormOrigin *gorm.DB
	Mongo      *mongo.Database
}
