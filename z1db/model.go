// Code generated by github.com/cascax/sql2gorm
package z1db

type Role struct {
	Z1Model
	Name   string `gorm:"column:name" json:"name" bson:"name"`
	Des    string `gorm:"column:des" json:"des" bson:"des"`
	Action string `gorm:"column:action" json:"action" bson:"action"`
	Status int64  `gorm:"column:status;default:1" json:"status" bson:"status"`
}

type User struct {
	Z1Model
	Username    string `gorm:"column:username" json:"username" bson:"username"`
	Email       string `gorm:"column:email" json:"email" bson:"email"`
	MobilePhone string `gorm:"column:mobile_phone" json:"mobile_phone" bson:"mobile_phone"`
	Avatar      string `gorm:"column:avatar" json:"avatar" bson:"avatar"`
	Password    string `gorm:"column:password" json:"password" bson:"password"`
	ApiToken    string `gorm:"column:api_token" json:"api_token" bson:"api_token"`
	Status      int64  `gorm:"column:status;default:1" json:"status" bson:"status"`
	Rid         int64  `gorm:"column:rid;default:1" json:"rid" bson:"rid"`
}

type UserRole struct {
	Z1Model
	Uid    int64 `gorm:"column:uid" json:"uid" bson:"uid"`
	Rid    int64 `gorm:"column:rid" json:"rid" bson:"rid"`
	Status int64 `gorm:"column:status;default:1" json:"status" bson:"status"`
}

type MongoRole struct {
	Z1Model
	Name   string `gorm:"column:name" json:"name" bson:"name"`
	Des    string `gorm:"column:des" json:"des" bson:"des"`
	Action string `gorm:"column:action" json:"action" bson:"action"`
	Status int64  `gorm:"column:status;default:1" json:"status" bson:"status"`
}

type MongoUser struct {
	Z1Model
	Username    string `gorm:"column:username" json:"username" bson:"username"`
	Email       string `gorm:"column:email" json:"email" bson:"email"`
	MobilePhone string `gorm:"column:mobile_phone" json:"mobile_phone" bson:"mobile_phone"`
	Avatar      string `gorm:"column:avatar" json:"avatar" bson:"avatar"`
	Password    string `gorm:"column:password" json:"password" bson:"password"`
	ApiToken    string `gorm:"column:api_token" json:"api_token" bson:"api_token"`
	Status      int64  `gorm:"column:status;default:1" json:"status" bson:"status"`
	Rid         int64  `gorm:"column:rid;default:1" json:"rid" bson:"rid"`
}

type MongoUserRole struct {
	Z1Model
	Uid    int64 `gorm:"column:uid" json:"uid" bson:"uid"`
	Rid    int64 `gorm:"column:rid" json:"rid" bson:"rid"`
	Status int64 `gorm:"column:status;default:1" json:"status" bson:"status"`
}
