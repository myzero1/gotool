package z1db

import (
	"encoding/json"
	"fmt"
	"log"
	"reflect"
	"strings"

	"gitee.com/myzero1/gotool/z1err"
	"gitee.com/myzero1/gotool/z1mongo"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"gopkg.in/mgo.v2/bson"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"gorm.io/plugin/soft_delete"
)

func Z1ToDryRun(db *gorm.DB) {

	if db.Statement.Model != nil {
		m, ok := db.Statement.Model.(Z1Modeli)
		if ok && m.DBType() == `mongo` {
			{
				flag := fmt.Sprintf(`{"_id":"%v"}`, `222222222222222222222222`)
				if db.DryRun {
					flag = fmt.Sprintf(`{"_id":"%v"}`, `111111111111111111111111`)
				}

				err := json.Unmarshal([]byte(flag), db.Statement.Model)
				z1err.Check(err)
			}

			db.DryRun = true
		}
	}
}

func Z1ToMongo(db *gorm.DB) {
	if db.DryRun {
		if db.Statement.Model != nil {
			m, ok := db.Statement.Model.(Z1Modeli)
			if ok && m.DBType() == `mongo` {
				{
					b, err := json.Marshal(db.Statement.Model)
					z1err.Check(err)
					bStr := string(b)
					dryRun := strings.Contains(bStr, `"_id":"111111111111111111111111"`)
					// log.Println(`--------------------`, bStr, dryRun)

					defer func() {
						if !dryRun {
							db.DryRun = false // Must at last
						}
					}()

					if !dryRun {
						stmt := db.Statement
						sql := db.Dialector.Explain(stmt.SQL.String(), stmt.Vars...)
						isCount := false
						z1ret := stmt.Dest

						// log.Println(`------sql--1--`, sql)

						if strings.HasPrefix(sql, `SELECT `) {
							if strings.Contains(sql, `count(`) {
								isCount = true
							}

							{
								b, err := json.Marshal(stmt.Dest)
								if err != nil {
									db.Error = err
									return
								}
								destStr := string(b)
								// log.Println(`------------destStr----------`, destStr)
								if !strings.HasPrefix(destStr, `[`) {
									sql = sql + ` LIMIT 1`
								}
							}
						}

						// log.Println(`------sql--2--`, sql)

						_, total, _, err := z1mongo.Sql2Mongo(sql, isCount, z1ret)

						if err != nil {
							db.Error = err
							return
						}

						db.RowsAffected = total

						if isCount {
							stmt.Dest = &total
							return
						}
					}
				}
			}
		}
	}
}

func Z1ToDryRun1(db *gorm.DB, modelIsMongo bool) {
	// callbacks.go
	// for _, f := range p.fns

	log.Println(`------Z1ToDryRun----------`)

	if modelIsMongo {
		db.DryRun = true
	}
}

func Z1ToMongo1(db *gorm.DB, model interface{}, stmt *gorm.Statement, modelIsMongo bool) {
	log.Println(`------Z1ToMongo----------`)
	// {
	// 	sql := db.Dialector.Explain(stmt.SQL.String(), stmt.Vars...)
	// 	log.Println(`------sql--1--`, sql)
	// }

	if modelIsMongo {
		sql := db.Dialector.Explain(stmt.SQL.String(), stmt.Vars...)
		isCount := true
		isMany := true

		log.Println(`------sql--1--`, sql)

		if strings.HasPrefix(sql, `SELECT `) {
			if !strings.Contains(sql, `count(`) {
				isCount = false
				if !strings.Contains(sql, ` LIMIT `) {
					sql = sql + ` LIMIT 1`
				}
			}
		}

		// log.Println(`------sql--2--`, sql)

		ret, total, action, err := z1mongo.Sql2Mongo(sql, isCount)

		if err != nil {
			db.Error = err
			return
		}

		db.RowsAffected = total

		if action == `select` {
			if isMany {
				db.RowsAffected = int64(len(ret))
				b, err := bson.Marshal(ret)
				if err != nil {
					db.Error = err
					return
				}
				err = bson.Unmarshal(b, model)
				if err != nil {
					db.Error = err
					return
				}
			} else {
				if len(ret) > 0 {
					db.RowsAffected = 1
					b, err := bson.Marshal(ret[0])
					if err != nil {
						db.Error = err
						return
					}
					err = bson.Unmarshal(b, model)
					if err != nil {
						db.Error = err
						return
					}
				}
			}
		}

		db.DryRun = false
	}
}

func Z1ParsingModel(db *gorm.DB, model interface{}) (isMongo bool) {
	if db.DryRun {
		return false
	}

	if model != nil {
		m, ok := model.(Z1Modeli)
		if ok && m.DBType() == `mongo` {
			isMongo = true
		}
	}

	return
}

func Z1ParsingModelOld(model interface{}) (isMongo, isSlice bool) {
	if model != nil {
		b, err := json.Marshal(model)

		if err != nil {
			return
		}

		bStr := string(b)

		isSlice = strings.HasPrefix(bStr, `[{"`)

		isMongo = strings.Contains(bStr, `"_id":"000000000000000000000000"`)
	}

	return
}

type Z1Model struct {
	// https://gorm.io/docs/delete.html#Soft-Delete
	// https://blog.csdn.net/qq_41554118/article/details/125645663
	// https://blog.csdn.net/weixin_44718305/article/details/128207602

	// Model

	ID        int64                 `gorm:"column:id;primarykey" json:"id" bson:"id"`                                       // 默认自增，可以人为设置sonyflake machineid max 65536 2^16
	CreatedAt int64                 `gorm:"column:created_at;not null" json:"created_at" bson:"created_at"`                 // 创建时间戳
	UpdatedAt int64                 `gorm:"column:updated_at;not null" json:"updated_at" bson:"updated_at"`                 // 更新时间戳
	DeletedAt soft_delete.DeletedAt `gorm:"column:deleted_at;index;not null;default:0" json:"deleted_at" bson:"deleted_at"` // 删除时间戳 已经改为了int64

	ID_ primitive.ObjectID `gorm:"-:all" json:"_id" bson:"_id"` // for mongodb _id 这个字段是标识，是否使用MongoDB的
}

type Z1Modeli interface {
	DBType() string
}

func (m *Z1Model) DBType() string {
	return `mysql` // for mongo must return mongo
}

var Z1field2StatementWhere map[string][]Z1field

// Z1field2StatementWhere[`staff`]=[]Z1field{Z1field{Name:`palce_id`,Value:0}}
func ModifyStatementWhere3(stmt *gorm.Statement) {
	// C:\Users\woogle\go\pkg\mod\gorm.io\plugin\soft_delete@v1.0.3\soft_delete.go

	if c, ok := stmt.Clauses["WHERE"]; ok {
		if z1fields, ok := Z1field2StatementWhere[clause.CurrentTable]; ok {
			if where, ok := c.Expression.(clause.Where); ok && len(where.Exprs) > 1 {
				for _, expr := range where.Exprs {
					if orCond, ok := expr.(clause.OrConditions); ok && len(orCond.Exprs) == 1 {
						where.Exprs = []clause.Expression{clause.And(where.Exprs...)}
						c.Expression = where
						stmt.Clauses["WHERE"] = c
						break
					}
				}
			}

			for _, f := range z1fields {
				stmt.AddClause(clause.Where{Exprs: []clause.Expression{
					clause.Eq{Column: clause.Column{Table: clause.CurrentTable, Name: f.Name}, Value: f.Value},
				}})
			}
		}
	}
}

func ModifyStatementWhere2(stmt *gorm.Statement, fieldName string, fieldValue interface{}) {
	// C:\Users\woogle\go\pkg\mod\gorm.io\plugin\soft_delete@v1.0.3\soft_delete.go

	if c, ok := stmt.Clauses["WHERE"]; ok {
		if where, ok := c.Expression.(clause.Where); ok && len(where.Exprs) > 1 {
			for _, expr := range where.Exprs {
				if orCond, ok := expr.(clause.OrConditions); ok && len(orCond.Exprs) == 1 {
					where.Exprs = []clause.Expression{clause.And(where.Exprs...)}
					c.Expression = where
					stmt.Clauses["WHERE"] = c
					break
				}
			}
		}

		stmt.AddClause(clause.Where{Exprs: []clause.Expression{
			clause.Eq{Column: clause.Column{Table: clause.CurrentTable, Name: fieldName}, Value: fieldValue},
		}})
	}
}

type Z1field struct {
	Name  string
	Value interface{}
}

var ModifyStatementWhereSettingDefault ModifyStatementWhereSetting

type ModifyStatementWhereSetting struct {
	On             bool
	ExcludedTables string
	FieldName      string
	FieldValue     interface{}
}

// ModifyStatementWhereSettingDefault{On:false,ExcludedTables:`,user,role,`,FieldName:`place_id`,FieldValue:123456}
func ModifyStatementWhere(db *gorm.DB) {
	// C:\Users\woogle\go\pkg\mod\gorm.io\plugin\soft_delete@v1.0.3\soft_delete.go

	stmt := db.Statement
	if c, ok := stmt.Clauses["WHERE"]; ok {
		if ModifyStatementWhereSettingDefault.On && !strings.Contains(ModifyStatementWhereSettingDefault.ExcludedTables, `,`+clause.CurrentTable+`,`) {
			if where, ok := c.Expression.(clause.Where); ok && len(where.Exprs) > 1 {
				for _, expr := range where.Exprs {
					if orCond, ok := expr.(clause.OrConditions); ok && len(orCond.Exprs) == 1 {
						where.Exprs = []clause.Expression{clause.And(where.Exprs...)}
						c.Expression = where
						stmt.Clauses["WHERE"] = c
						break
					}
				}
			}

			stmt.AddClause(clause.Where{Exprs: []clause.Expression{
				clause.Eq{
					Column: clause.Column{
						Table: clause.CurrentTable,
						Name:  ModifyStatementWhereSettingDefault.FieldName,
					},
					Value: ModifyStatementWhereSettingDefault.FieldValue,
				},
			}})
		}
	}
}

func ModifyStatementCreate(db *gorm.DB) {
	// https://gorm.io/zh_CN/docs/write_plugins.html

	if db.Statement.Schema != nil {
		if ModifyStatementWhereSettingDefault.On && !strings.Contains(ModifyStatementWhereSettingDefault.ExcludedTables, `,`+clause.CurrentTable+`,`) {
			switch db.Statement.ReflectValue.Kind() {
			case reflect.Slice, reflect.Array:
				for i := 0; i < db.Statement.ReflectValue.Len(); i++ {
					field := db.Statement.Schema.LookUpField(ModifyStatementWhereSettingDefault.FieldName)
					if field != nil {
						if _, isZero := field.ValueOf(db.Statement.ReflectValue.Index(i)); isZero {
							err := field.Set(db.Statement.ReflectValue.Index(i), ModifyStatementWhereSettingDefault.FieldValue)
							z1err.Check(err)
						}
					}
				}
			case reflect.Struct:
				field := db.Statement.Schema.LookUpField(ModifyStatementWhereSettingDefault.FieldName)
				if field != nil {
					if _, isZero := field.ValueOf(db.Statement.ReflectValue); !isZero {
						err := field.Set(db.Statement.ReflectValue, ModifyStatementWhereSettingDefault.FieldValue)
						z1err.Check(err)
					}
				}
			}
		}
		// crop image fields and upload them to CDN, dummy code
		// for _, field := range db.Statement.Schema.Fields {
		// 	switch db.Statement.ReflectValue.Kind() {
		// 	case reflect.Slice, reflect.Array:
		// 		for i := 0; i < db.Statement.ReflectValue.Len(); i++ {
		// 			// Get value from field
		// 			if _, isZero := field.ValueOf(db.Statement.ReflectValue.Index(i)); isZero {
		// 				// 添加内容
		// 			}
		// 		}
		// 	case reflect.Struct:
		// 		if _, isZero := field.ValueOf(db.Statement.ReflectValue); !isZero {
		// 			field := db.Statement.Schema.LookUpField("Name")
		// 			err := field.Set(db.Statement.ReflectValue, "newValue")
		// 			z1err.Check(err)
		// 		}
		// 	}
		// }

		// // All fields for current model
		// db.Statement.Schema.Fields

		// // All primary key fields for current model
		// db.Statement.Schema.PrimaryFields

		// // Prioritized primary key field: field with DB name `id` or the first defined primary key
		// db.Statement.Schema.PrioritizedPrimaryField

		// // All relationships for current model
		// db.Statement.Schema.Relationships

		// // Find field with field name or db name
		// field := db.Statement.Schema.LookUpField("Name")

		// // processing
	}
}
