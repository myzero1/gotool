# gotool

#### 介绍

golang 工具集

#### 软件架构

软件架构说明

#### 安装教程

1.  bash z1db/scripts/z1db-install.sh
2.  xxxx
3.  xxxx

#### 使用说明

1.  bash z1db/scripts/z1db-install.sh 安装 z1db
2.  bash z1db/scripts/z1sql2grom.sh 把sql转为struct,包含gorm、json、bson标签和DBType实现
3.  xxxx
4.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 特技

1.  使用 Readme_XXX.md 来支持不同的语言，例如 Readme_en.md, Readme_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

### z1db

***sql2mongo支持的sql类型***

|  sql类型   | 实例  | 说明  |
|  ----  | ----  | ----  |
| insert  | `INSERT INTO role (name, des, CreatedAt) VALUES ('z1', 'myzero1', 1684654029),('z12', 'myzero12',1684654033)` |可以单条和多条插入
| delete  | `DELETE FROM user WHERE name = 'z1'` |删除满足条件的所有记录
| drop table  | `DROP TABLE user` |删除user集合
| select  | `SELECT user.username,user.id FROM user WHERE username LIKE "myzero1%"` |字段名称前面都加上表名称
| join  | `SELECT user.username,role.des FROM user JOIN role ON user.rid=role.id WHERE user.username LIKE "myzero1%"` |关联查询支持join和left join
| join3  | `SELECT user.username,role.des FROM user_role JOIN role ON user_role.rid=role.id JOIN role ON user_role.uid=user.id WHERE user.username LIKE "myzero1%"` |三个表关联查询，主表必须包含其它2个表的关联字段


***z1db使用gorm操作mongodb数据库的方法列表***

`安装说明：使用z1db前需要先运行go\pkg\mod\gitee.com\myzero1\gotool@v0.9.0\z1db.sh脚本替换grom的包，否则mongodb无效`

`需要使用mongdb，需要让对应的model实现DBType方法，请参考MongoUser   go\pkg\mod\gitee.com\myzero1\gotool@v0.9.0\z1db\model.go`

详情参考 go\pkg\mod\gitee.com\myzero1\gotool@v0.9.0\test\z1db_test.go

* create

```

实例：
		user := z1db.MongoUser{
			Username: `gorm_username`,
			Email:    `gorm_email@qq.com`,
		}
		tx := db.Model(&z1db.MongoUser{}).Create(&user)
		// tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--1---`, tx.RowsAffected)
		}

对应sql：
INSERT INTO `mongo_users` (
	`created_at`,
	`updated_at`,
	`deleted_at`,
	`username`,
	`email`,
	`mobile_phone`,
	`avatar`,
	`password`,
	`api_token`,
	`status`,
	`rid`
)
VALUES
	(
		1684986904,
		1684986904,
		NULL,
		'gorm_username',
		'gorm_email@qq.com',
		'',
		'',
		'',
		'',
		1,
		1
	)

说明：


```

* delete

```

实例：
		tx := db.Model(&z1db.MongoUser{}).Where("username = ?", `gorm_username2`).Delete(&z1db.MongoUser{})
		// tx := db.Where("username = ?", `gorm_username2`).Delete(&z1db.MongoUser{})

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--Delete-----`, tx.RowsAffected)
		}

对应sql：
UPDATE `mongo_users`
SET `deleted_at` = '2023-05-25 11:55:04.669'
WHERE
	username = 'gorm_username2'
AND `mongo_users`.`deleted_at` IS NULL

说明：


```

* update

```

实例：
		tx := db.Model(&z1db.MongoUser{}).Where(`username=?`, `gorm_username`).Updates(z1db.MongoUser{Username: `gorm_username-updated`})

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--update-----`, tx.RowsAffected)
		}

对应sql：
UPDATE `mongo_users`
SET `updated_at` = 1684986904,
 `username` = 'gorm_username-updated'
WHERE
	username = 'gorm_username'
AND `mongo_users`.`deleted_at` IS NULL

说明：


```


* select one

```

实例：
		user := z1db.MongoUser{}
		tx := db.Model(&z1db.MongoUser{}).Where("username = ?", `gorm_username-updated`).Find(&user)
		// tx := db.Model(&z1db.MongoUser{}).Select("username").Where("username = ?", `gorm_username-updated`).Find(&user)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select----one-----`, tx.RowsAffected)
			t.Log(`----Result---Select-----one----`, user)
		}

对应sql：
SELECT
	`username`
FROM
	`mongo_users`
WHERE
	username = 'gorm_username'
AND `mongo_users`.`deleted_at` IS NULL
LIMIT 1

说明：
user 必须声明为实例（user := z1db.MongoUser{}），不能只声明不实例（var user z1db.MongoUser）

```


* select many

```

实例：
		users := []z1db.MongoUser{
			z1db.MongoUser{},
		}

		tx := db.Model(&z1db.MongoUser{}).Where("username like ?", `gorm_username%`).Offset(1).Limit(20).Find(&users)
		// tx := db.Model(&z1db.MongoUser{}).Select("username").Where("username like ?", `gorm_username-updated`).Offset(1).Limit(20).Find(&users)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select---many-----`, tx.RowsAffected)
			t.Log(`----Result---Select----many----`, users)
		}

对应sql：
SELECT
	`username`
FROM
	`mongo_users`
WHERE
	username = 'gorm_username'
AND `mongo_users`.`deleted_at` IS NULL

说明：
users 必须声明为实例（users := []z1db.MongoUser{z1db.MongoUser{}}），不能只声明不实例（var users []z1db.MongoUser）

```


* select count

```

实例：
		var count int64

		tx := db.Model(&z1db.MongoUser{}).Where("username like ?", `gorm_username%`).Count(&count)
		// tx := db.Model(&z1db.MongoUser{}).Select("username").Where("username like ?", `gorm_username%`).Count(&count)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select---count-----`, tx.RowsAffected)
			t.Log(`----RowsAffected---Select---count-----`, count)
		}

对应sql：
SELECT
	count(*)
FROM
	`mongo_users`
WHERE
	(
		username LIKE 'gorm_username%'
		OR created_at > 0
	)
AND `mongo_users`.`deleted_at` IS NULL

说明：

```

#### log

```
0.10	mongodb for gorm without rewriting gorm, 
		and add transaction
		add callback function for user


```