# myzero1@qq.com

# readInput "请输入环境ID(两位数字，回车默认为11)" "你入环境的ID错误" "/^[0-9]\{2,2\}$/p"
function readInput(){
    msg=$1
    errMsg=$2
    pattern=$3
    default=$4

    for ((VAR=1;;VAR++))
    do
        # read  -p "请输入环境ID(两位数字，回车默认为11)：" id
        read  -p "${msg}: " envId
        if [ -z "${envId}" ];then
            envId=$default
        fi

        if [ -n "$(echo $envId| sed -E -n ${pattern})" ];then
            break
        else
            # echo -e "\033[40;32m \xE2\x9C\x94 对 \033[0m" # 绿勾勾
            # echo -e "\033[40;31m \xE2\x9D\x8C 错 \033[0m" # 红叉叉
            echo -e "\033[40;31m \xE2\x9D\x8C ${errMsg}[${envId}] \033[0m" # 红叉叉
        fi
    done
}

function checkSql2Grom(){
    # https://gitee.com/z1-mirror/sql2gorm
    # https://sql2gorm.mccode.info/
    # https://github.com/cascax/sql2gorm
    # https://blog.csdn.net/jarvan5/article/details/117917790

    echo "====== checkSql2Grom start ====== "`date`


    if [ "$(command -v sql2gorm)" == "" ];then
        go get github.com/cascax/sql2gorm/...@master
        go install  github.com/cascax/sql2gorm/...@master
    fi


    echo "====== checkSql2Grom end ====== "`date`
}

function main(){
    readInput "请输sql文件路径(有效的linux文件路径,回车默认为 ../db/gin_best_practices.sql)" "你入的sql文件路径错误" "/^\.{0,2}(\/[-0-9a-zA-Z_.]+)+$/p" "../db/gin_best_practices.sql"
    z1SqlPath=$envId
    readInput "请输model文件路径(有效的linux文件路径,回车默认为 ../internal/model/gin_best_practices.go)" "你入的model文件路径错误" "/^\.{0,2}(\/[-0-9a-zA-Z_.]+)+$/p" "../internal/model/gin_best_practices.go"
    z1ModelPath=$envId

    echo '######################################'
    echo -e "\033[40;32m \xE2\x9C\x94 你输入的sql文件路径为: ${z1SqlPath} \033[0m" # 绿勾勾
    echo -e "\033[40;32m \xE2\x9C\x94 你输入的model文件路径为: ${z1ModelPath} \033[0m" # 绿勾勾
    echo '------ 关于sql2gorm的更多使用方法,请参考https://gitee.com/z1-mirror/sql2gorm ------'
    echo '######################################'

    checkSql2Grom

    echo "====== sql2Grom start ====== "`date`

    rm -rf $z1ModelPath
    sql2gorm -f $z1SqlPath -o $z1ModelPath -json

    # add bson
    # sed 's/json\(.*"\)/json\1 bson\1/' eg.go
    # sed 's/json\(.*\)"/json\1" bson\1"/' eg.go

    sed -i 's/json\(.*\)"/json\1" bson\1"/' $z1ModelPath

    # modify id,created_at,updated_at,DeletedAt
    # sed "/DeletedAt /d" eg.go
    sed -i "/ ID /d" $z1ModelPath
    sed -i "/\tID /d" $z1ModelPath
    
    sed -i "/ CreatedAt /d" $z1ModelPath
    sed -i "/\tCreatedAt /d" $z1ModelPath

    sed -i "/ UpdatedAt /d" $z1ModelPath
    sed -i "/\tUpdatedAt /d" $z1ModelPath

    sed -i "/ DeletedAt /d" $z1ModelPath
    sed -i "/\tDeletedAt /d" $z1ModelPath

    sed -i "/ ID_ /d" $z1ModelPath
    sed -i "/\tID_ /d" $z1ModelPath

    # add gorm.Z1Model
    # sed "/ struct {$/a\    gorm.Z1Model" eg.go

    # sed -i "/ struct {$/a\    \/\/ ----------------" $z1ModelPath
    # sed -i "/ struct {$/a\    ID_ primitive.ObjectID \`gorm:\"-:all\" json:\"_id\" bson:\"_id\"\`" $z1ModelPath
    # sed -i "/ struct {$/a\    DeletedAt gorm.DeletedAt \`gorm:\"column:deleted_at;index;not null;default:0\" json:\"deleted_at\" bson:\"deleted_at\"\`" $z1ModelPath
    # sed -i "/ struct {$/a\    UpdatedAt int64     \`gorm:\"column:updated_at;not null\" json:\"updated_at\" bson:\"updated_at\"\`" $z1ModelPath
    # sed -i "/ struct {$/a\    CreatedAt int64     \`gorm:\"column:created_at;not null\" json:\"created_at\" bson:\"created_at\"\`" $z1ModelPath
    # sed -i "/ struct {$/a\    ID        int64     \`gorm:\"column:id;primarykey\" json:\"id\" bson:\"id\"\`" $z1ModelPath
    sed -i "/ struct {$/a\    z1db.Z1Model" $z1ModelPath

    # add import
    # sed -i "/^package /a\ \nimport (\n \"go.mongodb.org/mongo-driver/bson/primitive\"\n \"gorm.io/gorm\"\n)\nfunc init() {\n _=gorm.Z1Model{} \/\/ err: undefined: gorm.Z1Model, run: \$(go env GOMODCACHE)/\$(go mod graph | grep 'gitee.com/myzero1/gotool' | head -n 2 | tail -n 1 | sed 's/ .*//')/z1db/scripts/z1db-install.sh\n}" $z1ModelPath
    sed -i "/^package /a\ \nimport (\n \"gitee.com/myzero1/gotool/z1db\"\n)" $z1ModelPath

    # add interface
    # sed -E "s/^type (.*) struct/func \(m \*\1\) DBType\(\) string {\n    \/\/ return \"mongo\"\n}\ntype \1 struct/" eg.go
    # sed -i -E "s/^type (.*) struct/func \(m \*\1\) DBType\(\) string {\n    \/\/ return \"mongo\" \/\/ Using mongo requires opening comment\n    return \"\"\n}\n\ntype \1 struct/" $z1ModelPath

    # Unified data type
    sed -i 's/ int / int64 /' $z1ModelPath
    sed -i 's/ int32 / int64 /' $z1ModelPath
    sed -i 's/ float / float64 /' $z1ModelPath
    sed -i 's/ float32 / float64 /' $z1ModelPath

    # primary_key
    sed -i 's/primary_key;default:.*json:/primary_key" json:/' $z1ModelPath

    echo "====== sql2Grom end ====== "`date`
}

main