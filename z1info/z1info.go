package z1info

import (
	"fmt"
)

var buildInfo string

// ShowBuildInfo show build infos.
// usage eg: go build -ldflags "-X gitee.com/myzero1/gotool/z1info.buildInfo=buildTime:`date +%Y-%m-%d,%H:%M:%S`;buildVersion:1.0.0;gitCommintID:`git rev-parse HEAD`" -o main.exe main.go
func ShowBuildInfo() {

	// go build -ldflags "-X gitee.com/myzero1/gotool/z1info.buildInfo=buildTime:`date +%Y-%m-%d,%H:%M:%S`;buildVersion:1.0.0;gitCommintID:`git rev-parse HEAD`" -o main.exe main.go

	// go build -ldflags "-X main.buildTime=`date +%Y-%m-%d,%H:%M:%S` -X main.buildVersion=1.0.0 -X main.gitCommintID=`git rev-parse HEAD`" -o main.exe main.go

	// go build \
	// -ldflags " \
	// -X main.buildTime=`date +%Y-%m-%d,%H:%M:%S` \
	// -X main.buildVersion=1.0.0  \
	// -X main.gitCommintID=`git rev-parse HEAD` \
	// " \
	// -o main.exe \
	// main.go

	fmt.Printf("################## build_info ##################\n")
	fmt.Printf("%s\n", buildInfo)
	fmt.Printf("################################################\n\n")
}
