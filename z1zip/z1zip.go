package z1zip

import (
	"archive/zip"
	"io"
	"os"
	"path/filepath"
	"strings"

	"gitee.com/myzero1/gotool/z1err"

	ezip "github.com/alexmullins/zip"
	hzip "github.com/dablelv/go-huge-util/zip"
)

// https://blog.csdn.net/weixin_43529465/article/details/128790732
// https://dablelv.blog.csdn.net/article/details/122441250
// https://github.com/yeka/zip
// https://github.com/dablelv/go-huge-util

func Zip(zipPath string, paths ...string) error {
	// 注意，该函数不支持符号链接，如果是符号链接将被忽略。
	return hzip.Zip(zipPath, paths...)
}

func ZipFollowSymlink(zipPath string, paths ...string) error {
	hzip.ZipFollowSymlink("archive.zip", "dir", "baz.csv")
	// // 该函数支持符号链接。
	return hzip.ZipFollowSymlink(zipPath, paths...)
}

func UnZip(zipPath string, dstDir string) error {
	return hzip.Unzip(zipPath, dstDir)
}

func ZipEncrypt(srcDir, dstDir, password string) error {
	src, err := filepath.Abs(srcDir)
	z1err.Check(err)
	desc, err := filepath.Abs(dstDir)
	z1err.Check(err)

	zipfile, err := os.Create(desc)
	if err != nil {
		return err
	}
	defer zipfile.Close()

	archive := ezip.NewWriter(zipfile)
	defer archive.Close()

	filepath.Walk(src, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		header, err := ezip.FileInfoHeader(info)
		if err != nil {
			return err
		}
		// srcPath := filepath.Join(filepath.Dir(src), filepath.Base(src)) + string(filepath.Separator)
		srcPath := filepath.Dir(src) + string(filepath.Separator)
		header.Name = strings.TrimPrefix(path, srcPath)
		// log.Println(`------------`, header.Name, path, filepath.Dir(src))
		if info.IsDir() {
			header.Name += "/"
		} else {
			header.Method = zip.Deflate
		}
		// 设置密码
		header.SetPassword(password)
		writer, err := archive.CreateHeader(header)
		if err != nil {
			return err
		}
		if !info.IsDir() {
			file, err := os.Open(path)
			if err != nil {
				return err
			}
			defer file.Close()
			_, err = io.Copy(writer, file)
		}
		return err
	})
	return err
}

func UnZipDecrypt(src, dstDir, password string) error {
	src, err := filepath.Abs(src)
	z1err.Check(err)
	desc, err := filepath.Abs(dstDir)
	z1err.Check(err)

	zipDir := filepath.Dir(src)

	r, err := ezip.OpenReader(src)
	z1err.Check(err)
	defer r.Close()

	for _, f := range r.File {
		// newName := desc + strings.TrimLeft(f.Name, `.`)
		newName := filepath.Join(desc, strings.Replace(f.Name, zipDir, ``, 1))

		if f.FileInfo().IsDir() {
			os.MkdirAll(newName, 0777)
		} else {
			of, err := os.Create(newName)
			z1err.Check(err)
			f.SetPassword(password)
			rr, err := f.Open()
			z1err.Check(err)
			_, err = io.Copy(of, rr)
			z1err.Check(err)
		}
	}

	return err
}
