module gitee.com/myzero1/gotool

go 1.13

require (
	github.com/alexmullins/zip v0.0.0-20180717182244-4affb64b04d0
	github.com/blastrain/vitess-sqlparser v0.0.0-20201030050434-a139afbb1aba
	github.com/cascax/sql2gorm v0.0.2 // indirect
	github.com/cloudquery/sqlite v1.0.1
	github.com/dablelv/go-huge-util v0.0.35
	github.com/mojocn/base64Captcha v1.3.1
	github.com/pkg/errors v0.9.1
	github.com/spf13/viper v1.10.1
	go.mongodb.org/mongo-driver v1.11.6
	go.uber.org/zap v1.19.1
	golang.org/x/sys v0.8.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
	gorm.io/driver/mysql v1.1.3
	gorm.io/gorm v1.21.16
	gorm.io/plugin/soft_delete v1.0.3
)

// https://studygolang.com/articles/23008
// go mod edit -replace github.com/mojocn/base64Captcha@v1.3.1=github.com/myzero1/fork2base64Captcha@z1.1.1
// replace github.com/mojocn/base64Captcha v1.3.1 => github.com/myzero1/fork2base64Captcha z1.1.1
// replace github.com/mojocn/base64Captcha v1.3.1 => github.com/myzero1/fork2base64Captcha v1.1.2.z1
replace github.com/mojocn/base64Captcha v1.3.1 => github.com/myzero1/fork2base64Captcha v1.3.2-0.20201207064552-63c342aa0bbd

// replace gorm.io/gorm v1.21.16 => github.com/myzero1/gorm v1.21.1606
