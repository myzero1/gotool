package z1mongo

import (
	"time"

	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Model interface {
	TableName() string
	DBType() string
}

type Z1Model struct {
	// *gorm.Model
}

func (m *Z1Model) DBType() string {
	return "mysql" // mongodb
}

type User struct {
	*Z1Model
	Username    string             `gorm:"column:username" json:"username" bson:"username,omitempty"`             // 用户名
	Email       string             `gorm:"column:email" json:"email" bson:"email,omitempty"`                      // 邮箱
	MobilePhone string             `gorm:"column:mobile_phone" json:"mobile_phone" bson:"mobile_phone,omitempty"` // 手机号
	Avatar      string             `gorm:"column:avatar" json:"avatar" bson:"avatar,omitempty"`                   // 头像地址
	Password    string             `gorm:"column:password" json:"password" bson:"password,omitempty"`             // 加密后的密码
	ApiToken    string             `gorm:"column:api_token" json:"api_token" bson:"api_token,omitempty"`          // 暂时保留，可能被删除
	Status      int64              `gorm:"column:status;default:1" json:"status" bson:"status,omitempty"`         // 状态 0为禁用、1为启用
	Rid         int64              `gorm:"column:rid;default:1" json:"rid" bson:"rid,omitempty"`                  // 状态 0为禁用、1为启用
	ID          int64              `gorm:"column:id;primary_key" json:"id" bson:"id,omitempty"`                   // sonyflake machineid max 65536 2^16
	CreatedAt   int64              `gorm:"column:created_at" json:"created_at" bson:"created_at,omitempty"`       // 创建时间戳
	UpdatedAt   int64              `gorm:"column:updated_at" json:"updated_at" bson:"updated_at,omitempty"`       // 更新时间戳
	DeletedAt   time.Time          `gorm:"column:deleted_at" json:"deleted_at" bson:"deleted_at,omitempty"`       // 删除时间 // for gorm
	ID_         primitive.ObjectID `gorm:"column:_id" json:"_id" bson:"_id,omitempty"`                            // for mongodb _id
}

func (m *User) TableName() string {
	return "user"
}

type User2 struct {
	*Z1Model
	Username string `gorm:"column:username" json:"username" bson:"username,omitempty"` // 用户名
	Email    string `gorm:"column:email" json:"email" bson:"email,omitempty"`          // 邮箱
}

func (m *User2) TableName() string {
	return "user2"
}

type Role struct {
	*Z1Model
	Name      string             `gorm:"column:name" json:"name" bson:"name,omitempty"`                   // 名称
	Des       string             `gorm:"column:des" json:"des" bson:"des,omitempty"`                      // 描述
	Action    string             `gorm:"column:action" json:"action" bson:"action,omitempty"`             // 操作，过个用逗号隔开
	Status    int64              `gorm:"column:status;default:1" json:"status" bson:"status,omitempty"`   // 状态 0为禁用、1为启用
	ID        int64              `gorm:"column:id;primary_key" json:"id" bson:"id,omitempty"`             // sonyflake machineid max 65536 2^16
	CreatedAt int64              `gorm:"column:created_at" json:"created_at" bson:"created_at,omitempty"` // 创建时间戳
	UpdatedAt int64              `gorm:"column:updated_at" json:"updated_at" bson:"updated_at,omitempty"` // 更新时间戳
	DeletedAt time.Time          `gorm:"column:deleted_at" json:"deleted_at" bson:"deleted_at,omitempty"` // 删除时间 // for gorm
	ID_       primitive.ObjectID `gorm:"column:_id" json:"_id" bson:"_id,omitempty"`                      // for mongodb _id
}

func (m *Role) TableName() string {
	return "role"
}

type UserRole struct {
	*Z1Model
	Uid       int64              `gorm:"column:uid" json:"uid" bson:"uid,omitempty"`                      // 名称
	Rid       int64              `gorm:"column:rid" json:"rid" bson:"rid,omitempty"`                      // 描述
	Status    int64              `gorm:"column:status;default:1" json:"status" bson:"status,omitempty"`   // 状态 0为禁用、1为启用
	ID        int64              `gorm:"column:id;primary_key" json:"id" bson:"id,omitempty"`             // sonyflake machineid max 65536 2^16
	CreatedAt int64              `gorm:"column:created_at" json:"created_at" bson:"created_at,omitempty"` // 创建时间戳
	UpdatedAt int64              `gorm:"column:updated_at" json:"updated_at" bson:"updated_at,omitempty"` // 更新时间戳
	DeletedAt time.Time          `gorm:"column:deleted_at" json:"deleted_at" bson:"deleted_at,omitempty"` // 删除时间 // for gorm
	ID_       primitive.ObjectID `gorm:"column:_id" json:"_id" bson:"_id,omitempty"`                      // for mongodb _id
}

func (m *UserRole) TableName() string {
	return "user_role"
}
