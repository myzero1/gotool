package z1mongo

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"

	"gitee.com/myzero1/gotool/z1err"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var DB *mongo.Database

// func NewZ1mongo(dbUri string, dbName string, timeout time.Duration, client_max_num uint64) {
func NewZ1mongo(cnf MongoConfig) {

	zero := MongoConfig{}
	if cnf.Timeout == zero.Timeout {
		cnf.Timeout = time.Second * 5
	}
	if cnf.ClientMaxNum == zero.ClientMaxNum {
		cnf.ClientMaxNum = 100
	}

	var err error

	condb := func() error {
		log.Println("----------------------ready connect to mongodb!")
		DB, err = Connect(cnf.Uri, cnf.DBName, cnf.Timeout, cnf.ClientMaxNum)
		if err != nil {
			log.Println("----------------------failed connectto mongodb!")
			return err
		}
		log.Println("----------------------connectted to mongodb!")

		return nil
	}
	condb()

	go func() {
		for {
			timeout := time.After(time.Second * 4)
			<-timeout
			if DB != nil {
				if err := DB.Client().Ping(context.Background(), readpref.Primary()); err != nil {
					fmt.Println(err)

					log.Println(" retry connect db")
					condb()
				}
			} else {
				condb()
			}
		}
	}()
}

func Add(model Model) (insertOneResult *mongo.InsertOneResult, err error) {
	b, err := bson.Marshal(model)
	z1err.Check(err)

	m := bson.M{}
	err = bson.Unmarshal(b, m)
	z1err.Check(err)
	if m[`_id`] != nil {
		if m[`_id`].(primitive.ObjectID).IsZero() {
			m[`_id`] = primitive.NewObjectID()
		}
	}

	collection := DB.Collection(model.TableName())
	insertOneResult, err = collection.InsertOne(context.TODO(), m)

	return
}

func AddMany(model Model, models []interface{}) (insertManyResult *mongo.InsertManyResult, err error) {
	collection := DB.Collection(model.TableName())
	insertManyResult, err = collection.InsertMany(context.TODO(), models)

	return
}

func One(model Model) (err error) {
	b, err := bson.Marshal(model)
	if err != nil {
		return
	}

	f := bson.M{}
	err = bson.Unmarshal(b, f)
	if err != nil {
		return
	}

	collection := DB.Collection(model.TableName())
	err = collection.FindOne(context.Background(), f).Decode(model)

	return
}

func Update(filter, model Model) (updateResult *mongo.UpdateResult, err error) {
	b, err := bson.Marshal(filter)
	if err != nil {
		return
	}

	f := bson.M{}
	err = bson.Unmarshal(b, f)
	if err != nil {
		return
	}

	collection := DB.Collection(model.TableName())

	d, err := bson.Marshal(model)
	z1err.Check(err)
	var model2 bson.M
	err = bson.Unmarshal(d, &model2)
	z1err.Check(err)

	update := bson.M{"$set": model2}
	updateResult, err = collection.UpdateOne(context.Background(), f, update)

	return
}

func Del(filter Model) (deleteResult *mongo.DeleteResult, err error) {
	b, err := bson.Marshal(filter)
	if err != nil {
		return
	}

	f := bson.M{}
	err = bson.Unmarshal(b, f)
	if err != nil {
		return
	}

	collection := DB.Collection(filter.TableName())

	deleteResult, err = collection.DeleteOne(context.Background(), filter)
	return
}

// bson.M{"name": primitive.Regex{Pattern: "深入"}}
// filer   bson.M{"username": "myzero1"}
// filer   bson.M{"username": bson.M{"$eq": "myzero1"}}
// filer   bson.M{"username": bson.M{"$regex": "my"}}
// filer   bson.M{"$and": []bson.M{bson.M{"created_at": bson.M{"$gt": 1684448021}}, bson.M{"created_at": bson.M{"$lte": 1684448124}}}},
func List(model Model, filter interface{}, page, pageSize int64, sort string, z1count bool) (ret []Model, total int64, err error) {
	collection := DB.Collection(model.TableName())

	otps := MakeFindOptions(page, pageSize, sort)

	cur, err := collection.Find(context.Background(), filter, otps)
	if err != nil {
		return
	}
	err = cur.Err()
	if err != nil {
		return
	}
	defer cur.Close(context.Background())

	if z1count {
		total, err = collection.CountDocuments(context.Background(), filter)
		if err != nil {
			return
		}
	} else {
		for cur.Next(context.Background()) {
			tmp := Deepcoper(model)
			err = cur.Decode(tmp)
			if err != nil {
				return
			}

			ret = append(ret, tmp)
		}
	}

	return
}

// MakeFindOptions(2, 3, `-update_time,+goods_id,created_time`)
func MakeFindOptions(page, pageSize int64, sort string) *options.FindOptions {
	//分页
	limit := pageSize
	skip := (page - 1) * pageSize

	sortD := bson.D{}
	info := strings.Split(sort, `,`)
	for _, v := range info {
		if string(v[0]) == `-` {
			t := bson.E{
				strings.Trim(v, `+-`),
				-1,
			}
			sortD = append(sortD, t)
		} else {
			t := bson.E{
				strings.Trim(v, `+-`),
				+1,
			}
			sortD = append(sortD, t)
		}
	}

	//查询条件
	opts := &options.FindOptions{
		Sort:  &sortD,
		Limit: &limit,
		Skip:  &skip,
	}

	return opts
}
