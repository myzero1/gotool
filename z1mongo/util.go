package z1mongo

import (
	"context"
	"fmt"
	"reflect"
	"time"

	"gitee.com/myzero1/gotool/z1err"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

// pool 连接池模式
func Connect(uri, name string, timeout time.Duration, num uint64) (*mongo.Database, error) {
	// 设置连接超时时间
	ctx, cancel := context.WithTimeout(context.Background(), timeout)
	defer cancel()
	// 通过传进来的uri连接相关的配置
	opts := options.Client().ApplyURI(uri)
	// 设置最大连接数 - 默认是100 ，不设置就是最大 max 64
	opts.SetMaxPoolSize(num)
	// 发起链接
	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		fmt.Println(err)
		return nil, err
	}
	// 判断服务是不是可用
	if err = client.Ping(context.Background(), readpref.Primary()); err != nil {
		fmt.Println(err)
		return nil, err
	}
	// 返回 client
	return client.Database(name), nil
}

func Disconnect() {
	if DB != nil {
		DB.Client().Disconnect(context.Background())
	}
}

func Deepcoper(m Model) Model {
	// 1. 获取 反射类型
	rt := reflect.TypeOf(m)

	// 2. 获取真实底层结构体的类型
	rtype := deRefType(rt)

	// 3. reflect.New() 创建反射对象，并使用 Interface() 转真实对象
	opc := reflect.New(rtype).Interface()

	// 4. 断言为 operator
	return opc.(Model)
}

func deRefType(typ reflect.Type) reflect.Type {
	for typ.Kind() == reflect.Ptr {
		typ = typ.Elem()
	}

	return typ
}

func BsonD2Mode(input interface{}, output Model) {
	b, err := bson.Marshal(input)
	z1err.Check(err)
	err = bson.Unmarshal(b, output)
	z1err.Check(err)
}

type MongoConfig struct {
	Uri          string //mongodb://localhost:27017, mongodb://username:password@localhost:27017
	DBName       string
	Timeout      time.Duration
	ClientMaxNum uint64
}
