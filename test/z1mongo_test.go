package test

import (
	"fmt"
	"log"
	"math/rand"
	"testing"
	"time"

	"gitee.com/myzero1/gotool/z1err"
	"gitee.com/myzero1/gotool/z1mongo"
	"gopkg.in/mgo.v2/bson"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

// go test -v .\test\z1mongo_test.go
func TestZ1mongo(t *testing.T) {
	t.Cleanup(setUpZ1mongo())

	// t.Run("TestRs", testRs)

	// t.Run("TestAdd", testAdd)
	// t.Run("TestAddMany", testAddMany)
	// t.Run("TestUpdate", testUpdate)
	// t.Run("TestOne", testOne)
	// t.Run("TestDel", testDel)
	// t.Run("TestList", testList)
	t.Run("TestSql2Mongo", testSql2Mongo)
}

func setUpZ1mongo() func() {

	z1mongo.NewZ1mongo(z1mongo.MongoConfig{
		Uri:    `mongodb://localhost:27017`,
		DBName: `z1mongo`,
	})

	{
		sql := `DROP TABLE role`
		_, _, _, err := z1mongo.Sql2Mongo(sql, false)
		z1err.Check(err)

		sql = `DROP TABLE user`
		_, _, _, err = z1mongo.Sql2Mongo(sql, false)
		z1err.Check(err)

		sql = `DROP TABLE user_role`
		_, _, _, err = z1mongo.Sql2Mongo(sql, false)
		z1err.Check(err)
	}

	{
		t := time.Now().Unix()
		for i := 1; i < 10; i++ {
			t2 := t + int64(i)
			tStr := fmt.Sprintf(`_%v`, t2)
			_, err := z1mongo.Add(
				&z1mongo.Role{
					ID:        int64(i),
					Name:      `name` + tStr,
					Des:       `des` + tStr,
					CreatedAt: t2,
				},
			)
			z1err.Check(err)
		}
	}

	{
		t := time.Now().Unix()
		for i := 1; i < 31; i++ {
			t2 := t + int64(i)
			tStr := fmt.Sprintf(`_%v`, t2)

			_, err := z1mongo.Add(
				&z1mongo.User{
					ID:        t2,
					Username:  `Username` + tStr,
					Rid:       rand.Int63n(10),
					CreatedAt: t2,
				},
			)
			z1err.Check(err)
		}
	}

	{
		t := time.Now().Unix()
		for i := 1; i < 46; i++ {
			t2 := t + int64(i)
			tStr := fmt.Sprintf(`_%v`, t2)

			_, err := z1mongo.Add(
				&z1mongo.User{
					ID:        t2,
					Username:  `Username` + tStr,
					Rid:       rand.Int63n(10),
					CreatedAt: t2,
				},
			)
			z1err.Check(err)
		}
	}

	{

		t := time.Now().Unix()
		for i := 1; i < 4; i++ {
			t2 := t + int64(i)
			t3 := 73500 + int64(i)
			tStr := fmt.Sprintf(`_%v`, t2)

			_, err := z1mongo.Add(
				&z1mongo.User{
					ID:        t3,
					Username:  `Username` + tStr,
					Rid:       rand.Int63n(9) + 1,
					CreatedAt: t2,
				},
			)
			z1err.Check(err)

			_, err = z1mongo.Add(
				&z1mongo.UserRole{
					ID:        t2,
					Uid:       t3,
					Rid:       rand.Int63n(9) + 1,
					CreatedAt: t2,
				},
			)
			z1err.Check(err)
		}
	}

	return func() {
		z1mongo.Disconnect()
	}
}

func testAdd(t *testing.T) {
	user := z1mongo.User{
		Username:  `testAdd`,
		CreatedAt: time.Now().Unix(),
		Rid:       5,
	}
	_, err := z1mongo.Add(&user)
	if err != nil {
		t.Error(err)
	}
}

func testAddMany(t *testing.T) {
	var users []interface{}
	users = append(
		users,
		&z1mongo.User{
			Username:  `testAdd1`,
			CreatedAt: time.Now().Unix(),
			Rid:       6,
		},
		&z1mongo.User{
			Username:  `testAdd2`,
			CreatedAt: time.Now().Unix(),
			Rid:       6,
		},
	)
	_, err := z1mongo.AddMany(&z1mongo.User{}, users)
	if err != nil {
		t.Error(err)
	}
}

func testOne(t *testing.T) {
	user := z1mongo.User{
		Username: `testAdd2-updated`,
	}
	err := z1mongo.One(&user)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(user)
	}
}

func testUpdate(t *testing.T) {
	filter := z1mongo.User{
		Username: `testAdd2`,
	}
	user := z1mongo.User{
		Username:  `testAdd2-updated`,
		UpdatedAt: time.Now().Unix(),
	}
	_, err := z1mongo.Update(
		&filter,
		&user,
	)

	if err != nil {
		t.Error(err)
	} else {
		t.Log(user.Username)
	}
}

func testDel(t *testing.T) {
	user := z1mongo.User{
		Username: `testAdd2`,
	}
	_, err := z1mongo.Del(&user)
	if err != nil {
		t.Error(err)
	}
}

func testList(t *testing.T) {
	_, _, err := z1mongo.List(
		&z1mongo.User{},
		// bson.M{"username": "myzero1"},
		bson.M{"username": bson.M{"$regex": "testAdd"}},
		// bson.M{"$and": []bson.M{bson.M{"created_at": bson.M{"$gt": 1684448021}}, bson.M{"created_at": bson.M{"$lte": 1684448150}}}},

		1,
		3,

		// `-username`,
		// `+username`,
		`username`,

		// true,
		false,
	)

	if err != nil {
		t.Error(err)
	}
}

func testSql2Mongo(t *testing.T) {
	// INSERT INTO `mongo_users` (`created_at`,`updated_at`,`deleted_at`,`username`,`email`,`mobile_phone`,`avatar`,`password`,`api_token`,`status`,`rid`) VALUES (1684911179,1684911179,NULL,'gorm_username','gorm_email@qq.com','','','','',1,1)
	// sql := `INSERT INTO role (name, des, CreatedAt) VALUES ('z1', 'myzero1', 1684654029),('z12', 'myzero12',1684654033)`
	// sql := `UPDATE user SET name = 'updated', updated_at = 1682723133 WHERE name = 'z1'`
	// sql := `DELETE FROM user WHERE name = 'z1'`
	// sql := `DROP TABLE user`
	sql := `SELECT user.username,role.des FROM user_role JOIN role ON user_role.rid=role.id JOIN user ON user_role.uid=user.id WHERE user.username LIKE "Username%"`

	n := time.Now().Unix()
	sql = fmt.Sprintf(`INSERT INTO user (id, username, rid, created_at) VALUES (%v,"username_insert_%v",1,%v),(%v,"2username_insert_%v",1,%v),(%v,"username_insert_update",1,%v)`, n, n, n, n, n, n, n, n)
	// like 会默认转成，先后模糊
	sql2 := `DELETE FROM user WHERE username like 'username_insert_%'`
	// sql2 := `UPDATE user SET username = 'updated', updated_at = 1683333133 WHERE username = 'username_insert_update'`
	// sql = `DROP TABLE user`

	sql = `INSERT INTO mongo_users (created_at,updated_at,deleted_at,username,email,mobile_phone,avatar,password,api_token,status,rid) VALUES (1684911457,1684911457,NULL,'gorm_username','gorm_email@qq.com','','','','',1,1),(1684911457,1684911457,NULL,'gorm_username','gorm_email@qq.com','','','','',1,1)`
	sql = `INSERT INTO mongo_roles (created_at,updated_at,deleted_at,name,des,action,status,id) VALUES (1685748294,1685748294,NULL,'role-join1','','',1,1001.12),(1685748294,1685748294,NULL,'role-join2','','',1,1002),(1685748294,1685748294,NULL,'role-join3','','',1,1003)`
	// sql = `SELECT * FROM mongo_roles`
	sql = `UPDATE role SET deleted_at='2023-05-25 09:55:45.033',updated_at=1685782735 WHERE id = 9`
	// sql = `SELECT mongo_users.username,mongo_roles.name FROM mongo_user_roles JOIN mongo_users ON mongo_user_roles.uid = mongo_users.id JOIN mongo_roles ON mongo_user_roles.rid = mongo_roles.id WHERE mongo_user_roles.deleted_at IS NULL`
	sql = `SELECT mongo_users.username,mongo_roles.name FROM mongo_user_roles JOIN mongo_users ON mongo_user_roles.uid = mongo_users.id JOIN mongo_roles ON mongo_user_roles.rid = mongo_roles.id WHERE mongo_user_roles.deleted_at IS NULL and mongo_roles.id<>1002`
	sql = `SELECT mongo_users.username,mongo_roles.name FROM mongo_user_roles JOIN mongo_users ON mongo_user_roles.uid = mongo_users.id JOIN mongo_roles ON mongo_user_roles.rid = mongo_roles.id WHERE mongo_user_roles.deleted_at IS NULL and mongo_roles.id!=1002`
	sql = `SELECT mongo_users.username,mongo_roles.name FROM mongo_user_roles JOIN mongo_users ON mongo_user_roles.uid = mongo_users.id left JOIN mongo_roles ON mongo_user_roles.rid = mongo_roles.id WHERE mongo_user_roles.deleted_at IS NULL and mongo_roles.id!=1002`
	sql = `SELECT mongo_users.username as mun,mongo_roles.name as nm FROM mongo_user_roles JOIN mongo_users ON mongo_user_roles.uid = mongo_users.id left JOIN mongo_roles ON mongo_user_roles.rid = mongo_roles.id WHERE mongo_user_roles.deleted_at =0 and mongo_roles.id!=1002`
	ret, _, _, err := z1mongo.Sql2Mongo(sql, false)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(`------Sql2Mongo ok--------`, ret)
		if len(ret) > 0 {
			r := ret[0]

			t.Log(ret, r, r[`username`], len(ret))
		}
	}

	return

	ret, _, _, err = z1mongo.Sql2Mongo(sql2, false)
	if err != nil {
		t.Error(err)
	} else {
		t.Log(`------Sql2Mongo ok--------`)
		if len(ret) > 0 {
			r := ret[0]

			t.Log(r, r[`username`])
		}
	}
}

func testRs(t *testing.T) {
	db, err := gorm.Open(mysql.New(mysql.Config{
		DSN:                       "root:@tcp(127.0.0.1:3306)/z1gorm?charset=utf8&parseTime=True&loc=Local", // DSN data source name
		DefaultStringSize:         256,                                                                      // string 类型字段的默认长度
		DisableDatetimePrecision:  true,                                                                     // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		DontSupportRenameIndex:    true,                                                                     // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		DontSupportRenameColumn:   true,                                                                     // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		SkipInitializeWithVersion: false,                                                                    // 根据当前 MySQL 版本自动配置
	}), &gorm.Config{})
	z1err.Check(err)

	user2 := z1mongo.User2{
		Username: `username_111`,
		Email:    `email_111`,
	}

	// result := db.Create(&user2) // pass a slice to insert multiple row
	// z1err.Check(result.Error)
	// result.Error        // returns error
	// result.RowsAffected // returns inserted records count

	// stmt := db.Session(&gorm.Session{DryRun: true}).Create(&user2).Statement
	stmt := db.Create(&user2).Statement
	// stmt.SQL.String() //=> SELECT * FROM `users` WHERE `id` = $1 ORDER BY `id`
	// stmt.Vars         //=> []interface{}{1}
	log.Println(stmt.SQL.String())
	// log.Fatal()

	// sql := db.ToSQL(func(tx *gorm.DB) *gorm.DB {
	// 	// return tx.Model(&z1mongo.User2{}).Create(&user2)
	// 	return tx.Create(&user2)
	// })

	// log.Println(`-------sql-----`, sql)

	db.Create(&user2)
	db.First(&user2)
	db.Take(&user2)
	db.Last(&user2)
	db.First(&user2, "id = ?", "string_primary_key")
	db.Raw("SELECT name, age FROM users WHERE name = ?", "Antonio").Scan(&user2)

}
