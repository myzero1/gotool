package test

import (
	"testing"

	"gitee.com/myzero1/gotool/z1cnf"
	"gitee.com/myzero1/gotool/z1log"
	"go.uber.org/zap"
)

func TestLog(t *testing.T) {
	z1log.Info("----------test split line,TestLog-----------")

	{
		z1cnf.Init(z1cnf.YAML, `config/config.yaml`, `contentType: yaml
成绩:
  张三: 90
  李四: 95
班级: 一年级
老师:  # 数学老师和语文老师
- 张老师
- 李老师`)
		z1log.Info(`test Init`, zap.String(`contentType`, z1cnf.Z1viper.GetString(`contentType`)))
	}

	{
		z1cnf.Init(z1cnf.JSON, `config/config.yaml`, `{"contentType":"json","name":"张三","年龄":18}`)
		z1log.Info(`test Init`, zap.String(`contentType`, z1cnf.Z1viper.GetString(`contentType`)))
	}

	{
		z1log.Info(`z1cnf.Convert`, zap.String(`targetContent`, z1cnf.Convert(`json`, `{"contentType":"json","name":"张三","年龄":18}`, `yaml`)))
	}
}
