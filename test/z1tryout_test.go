package test

import (
	"testing"

	"gitee.com/myzero1/gotool/z1log"
	"gitee.com/myzero1/gotool/z1tryout"
)

func TestTryoutSetUrl(t *testing.T) {
	z1log.Infoln("----------test split line,TestTryoutSetUrl-----------")
	z1tryout.SetTryout(`https://gitee.com/myzero1/tryout/raw/master/test/1111`)
	z1log.Infoln(`------z1tryout.TryoutExpired()-------`, z1tryout.TryoutExpired())
}

func TestTryout(t *testing.T) {
	z1log.Infoln("----------test split line,TestTryout-----------")
	z1tryout.SetTryout(`https://gitee.com/myzero1/tryout/raw/master/test/`)
	z1log.Infoln(`------z1tryout.TryoutExpired()-------`, z1tryout.TryoutExpired())
}

func TestTryoutWithDeadline(t *testing.T) {
	z1log.Infoln("----------test split line,TestTryoutWithDeadline-----------")
	z1tryout.SetTryout(`https://gitee.com/myzero1/tryout/raw/master/test/111`, `2022-01-02 15:04:05`)
	z1log.Infoln(`------z1tryout.TryoutExpired()-------`, z1tryout.TryoutExpired())
}
