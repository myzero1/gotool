package test

import (
	"context"
	"encoding/json"
	"log"
	"testing"
	"time"

	"gitee.com/myzero1/gotool/z1db"
	"gitee.com/myzero1/gotool/z1db/gormplugin"
	"gitee.com/myzero1/gotool/z1err"
	"gitee.com/myzero1/gotool/z1mongo"
	"gorm.io/gorm"
)

// go test -v .\test\z1mongo_test.go
func TestZ1Db(t *testing.T) {

	{
		t.Cleanup(z1dbSetup())
		t.Run("Testz1dbRs", z1dbRs)
		return
	}

	t.Cleanup(z1dbSetup())
	t.Run("Testz1dbMongoCreate", z1dbMongoCreate)
	time.Sleep(time.Second * 1)
	t.Run("Testz1dbMongoUpdate", z1dbMongoUpdate)
	t.Run("Testz1dbMongoFindOne", z1dbMongoFindOne)
	t.Run("Testz1dbMongoDelete", z1dbMongoDelete)
	t.Run("Testz1dbMongoFindMany", z1dbMongoFindMany)
	t.Run("Testz1dbMongoFindCount", z1dbMongoFindCount)
	t.Run("Testz1dbMongoJoin", z1dbMongoJoin)
	t.Run("Testz1dbGormCreate", z1dbGormCreate)
	time.Sleep(time.Second * 1)
	t.Run("Testz1dbGormUpdate", z1dbGormUpdate)
	t.Run("Testz1dbGormFindOne", z1dbGormFindOne)
	t.Run("Testz1dbGormDelete", z1dbGormDelete)
	t.Run("Testz1dbGormFindMany", z1dbGormFindMany)
	t.Run("Testz1dbGormFindCount", z1dbGormFindCount)
	t.Run("Testz1dbJoin", z1dbJoin)

}

func z1dbSetup() func() {

	z1db.NewDB(
		// z1mongo.MongoConfig{}
		z1mongo.MongoConfig{
			Uri:    `mongodb://localhost:27017`,
			DBName: `z1mongo`,
		},

		gormplugin.ToMongo{
			EnableTables: `,mongo_user,mongo_role,mongo_user_role,`,
		},

		gormplugin.AddStatement{
			EnableTables: `,mongo_user,mongo_role,mongo_user_role,user,role,user_role,`,
		},

		gorm.Config{},

		// sqlite.Open(`z1db/sqlite.db`),
		// mysql.New(mysql.Config{
		// 	DSN:                       "root:@tcp(127.0.0.1:3306)/z1gorm_test?charset=utf8&parseTime=True&loc=Local", // DSN data source name
		// 	DefaultStringSize:         256,                                                                           // string 类型字段的默认长度
		// 	DisableDatetimePrecision:  true,                                                                          // 禁用 datetime 精度，MySQL 5.6 之前的数据库不支持
		// 	DontSupportRenameIndex:    true,                                                                          // 重命名索引时采用删除并新建的方式，MySQL 5.7 之前的数据库和 MariaDB 不支持重命名索引
		// 	DontSupportRenameColumn:   true,                                                                          // 用 `change` 重命名列，MySQL 8 之前的数据库和 MariaDB 不支持重命名列
		// 	SkipInitializeWithVersion: false,
		// }),
	)

	// use AddStatement
	{
		z1db.DB.Gorm = z1db.DB.Gorm.WithContext(context.WithValue(
			context.Background(),
			`z1AddStatement`,
			map[string]interface{}{
				`status`: 735,
			},
		))
	}

	// gorm
	{
		db := z1db.DB.Gorm

		// http://www.17bigdata.com/study/programming/gorm/gorm-migration.html
		db.Migrator().DropTable(
			&z1db.User{},
			&z1db.Role{},
			&z1db.UserRole{},
		)

		if db.Dialector.Name() == `mysql` {
			db = db.Set("gorm:table_options", "ENGINE=InnoDB")
		}

		db.AutoMigrate(
			&z1db.User{},
			&z1db.Role{},
			&z1db.UserRole{},
		)
	}

	// mongo
	{
		{
			sql := `DROP TABLE mongo_role`
			_, _, _, err := z1mongo.Sql2Mongo(sql, false)
			z1err.Check(err)

			sql = `DROP TABLE mongo_user`
			_, _, _, err = z1mongo.Sql2Mongo(sql, false)
			z1err.Check(err)

			sql = `DROP TABLE mongo_user_role`
			_, _, _, err = z1mongo.Sql2Mongo(sql, false)
			z1err.Check(err)
		}
	}
	return func() {
		z1mongo.Disconnect()
	}
}

func z1dbMongoCreate(t *testing.T) {
	db := z1db.DB.Gorm

	// create
	{
		user := z1db.MongoUser{
			Username: `gorm_username`,
			Email:    `gorm_email@qq.com`,
			Z1Model: z1db.Z1Model{
				ID: 1,
			},
		}
		tx := db.Model(&z1db.MongoUser{}).Create(&user)
		// tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--1---`, tx.RowsAffected)
		}

		{
			txSql := db.Session(&gorm.Session{DryRun: true}).Model(&z1db.MongoUser{}).Create(&user)
			log.Println(`-------View the SQL that Gorm will execute---------`, db.Dialector.Explain(
				txSql.Statement.SQL.String(),
				txSql.Statement.Vars...,
			))
		}

		user = z1db.MongoUser{
			Username: `gorm_username2`,
			Email:    `gorm_email@qq.com`,
			Z1Model: z1db.Z1Model{
				ID: 2,
			},
		}
		tx = db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--2---`, tx.RowsAffected)
		}

		user = z1db.MongoUser{
			Username: `gorm_username3`,
			Email:    `gorm_email@qq.com`,
			Z1Model: z1db.Z1Model{
				ID: 3,
			},
		}
		tx = db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--3---`, tx.RowsAffected)
		}

		user = z1db.MongoUser{
			Username: `gorm_username4`,
			Email:    `gorm_email@qq.com`,
			Z1Model: z1db.Z1Model{
				ID: 4,
			},
		}
		tx = db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--4---`, tx.RowsAffected)
		}
	}

	// for join
	{
		users := []z1db.MongoUser{
			{
				Username: `gorm_username-join1`,
				Z1Model: z1db.Z1Model{
					ID: 1001,
				},
			},
			{
				Username: `gorm_username-join2`,
				Z1Model: z1db.Z1Model{
					ID: 1002,
				},
			},
			{
				Username: `gorm_username-join3`,
				Z1Model: z1db.Z1Model{
					ID: 1004,
				},
			},
		}

		{
			// txSql := db.Session(&gorm.Session{DryRun: true}).Model(&z1db.MongoUser{}).Create(&users)
			// log.Println(`-------View the SQL that Gorm will execute---------`, db.Dialector.Explain(
			// 	txSql.Statement.SQL.String(),
			// 	txSql.Statement.Vars...,
			// ))
		}

		tx := db.Model(&z1db.MongoUser{}).Create(&users)
		// tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--join--1---`, tx.RowsAffected)
		}

		roles := []z1db.MongoRole{
			{
				Name: `role-join1`,
				Z1Model: z1db.Z1Model{
					ID: 1001,
				},
			},
			{
				Name: `role-join2`,
				Z1Model: z1db.Z1Model{
					ID: 1002,
				},
			},
			{
				Name: `role-join3`,
				Z1Model: z1db.Z1Model{
					ID: 1003,
				},
			},
		}
		tx = db.Model(&z1db.MongoRole{}).Create(&roles)
		// tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--join--2---`, tx.RowsAffected)
		}

		userRoles := []z1db.MongoUserRole{
			{
				Uid: 1001,
				Rid: 1001,
			},
			{
				Uid: 1002,
				Rid: 1002,
			},
			{
				Uid: 1002,
				Rid: 1003,
			},
			{
				Uid: 1003,
				Rid: 1002,
			},
			{
				Uid: 735,
				Rid: 1002,
			},
			{
				Uid: 1003,
				Rid: 735,
			},
		}
		tx = db.Model(&z1db.MongoUserRole{}).Create(&userRoles)
		// tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--join--3---`, tx.RowsAffected)
		}

	}

}

func z1dbMongoDelete(t *testing.T) {
	db := z1db.DB.Gorm

	// deleted
	{
		// View the SQL that Gorm will execute
		{
			txSql := db.Session(&gorm.Session{DryRun: true}).Model(&z1db.MongoUser{}).Where("username = ?", `gorm_username2`).Delete(&z1db.MongoUser{})
			log.Println(`-------View the SQL that Gorm will execute---------`, db.Dialector.Explain(
				txSql.Statement.SQL.String(),
				txSql.Statement.Vars...,
			))
		}

		tx := db.Model(&z1db.MongoUser{}).Where("username = ?", `gorm_username2`).Delete(&z1db.MongoUser{})
		// tx := db.Where("username = ?", `gorm_username2`).Delete(&z1db.MongoUser{})

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--Delete-----`, tx.RowsAffected)
		}
	}
}

func z1dbMongoUpdate(t *testing.T) {
	db := z1db.DB.Gorm

	// update
	{
		{
			txSql := db.Session(&gorm.Session{DryRun: true}).Model(&z1db.MongoUser{}).Where(`username=?`, `gorm_username`).Updates(z1db.MongoUser{Username: `gorm_username-updated`})
			log.Println(`-------View the SQL that Gorm will execute---------`, db.Dialector.Explain(
				txSql.Statement.SQL.String(),
				txSql.Statement.Vars...,
			))
		}

		tx := db.Model(&z1db.MongoUser{}).Where(`username=?`, `gorm_username`).Updates(z1db.MongoUser{Username: `gorm_username-updated`})

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--update-----`, tx.RowsAffected)
		}
	}
}

func z1dbMongoFindOne(t *testing.T) {
	db := z1db.DB.Gorm

	// select one
	{
		user := z1db.MongoUser{}
		tx := db.Model(&z1db.MongoUser{}).Where("username = ?", `gorm_username-updated`).Find(&user)
		// tx := db.Model(&z1db.MongoUser{}).Select("username").Where("username = ?", `gorm_username-updated`).Find(&user)

		{
			txSql := db.Session(&gorm.Session{DryRun: true}).Model(&z1db.MongoUser{}).Where("username = ?", `gorm_username-updated`).Find(&user)
			log.Println(`-------View the SQL that Gorm will execute---------`, db.Dialector.Explain(
				txSql.Statement.SQL.String(),
				txSql.Statement.Vars...,
			))
		}

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select----one-----`, tx.RowsAffected)
			t.Log(`----Result---Select-----one----`, user)
		}
	}
}

func z1dbMongoFindMany(t *testing.T) {
	db := z1db.DB.Gorm

	// select
	{
		users := []z1db.MongoUser{}

		tx := db.Model(&z1db.MongoUser{}).Where("username like ?", `gorm_username%`).Offset(1).Limit(20).Find(&users)
		// tx := db.Model(&z1db.MongoUser{}).Select("username").Where("username like ?", `gorm_username-updated`).Offset(1).Limit(20).Find(&users)

		{
			txSql := db.Session(&gorm.Session{DryRun: true}).Model(&z1db.MongoUser{}).Where("username like ?", `gorm_username%`).Offset(1).Limit(20).Find(&users)
			log.Println(`-------View the SQL that Gorm will execute---------`, db.Dialector.Explain(
				txSql.Statement.SQL.String(),
				txSql.Statement.Vars...,
			))
		}

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select---many---RowsAffected--`, tx.RowsAffected)
			t.Log(`----Result---Select----many--users--`, users)
			t.Log(`----Result---Select----many--ID_--`, users[0].ID_)
		}
	}
}

func z1dbMongoJoin(t *testing.T) {
	db := z1db.DB.Gorm

	// join
	{
		joinMap := make([]map[string]interface{}, 0)

		tx := db.Session(&gorm.Session{}).Model(&z1db.MongoUserRole{}).Joins(
			"JOIN mongo_user ON mongo_user_role.uid = mongo_user.id",
		).Joins(
			"JOIN mongo_role ON mongo_user_role.rid = mongo_role.id",
		).Select(
			"mongo_user.username",
			"mongo_role.name",
			"mongo_user.id as id1",
			"mongo_role.id as id2",
			"role.des",
		).Find(&joinMap)

		{
			// https://gorm.io/docs/sql_builder.html#DryRun-Mode
			// https://gorm.io/docs/session.html#DryRun
			txSql := db.Session(&gorm.Session{DryRun: true}).Model(&z1db.MongoUserRole{}).Joins(
				"JOIN mongo_user ON mongo_user_role.uid = mongo_user.id",
			).Joins(
				"JOIN mongo_role ON mongo_user_role.rid = mongo_role.id",
			).Select(
				"mongo_user.username",
				"mongo_role.name",
				"mongo_user.id as id1",
				"mongo_role.id as id2",
			).Find(&joinMap)
			log.Println(`-------View the SQL that Gorm will execute---------`, db.Dialector.Explain(
				txSql.Statement.SQL.String(),
				txSql.Statement.Vars...,
			))
		}

		// tx := db.Model(&z1db.MongoUser{}).Select("username").Where("username like ?", `gorm_username%`).Count(&count)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			b, err := json.MarshalIndent(joinMap, ``, `  `)
			z1err.Check(err)
			t.Log(`----RowsAffected---join---1-----`, string(b))
			t.Log(`----RowsAffected---join---RowsAffected--`, tx.RowsAffected)
			t.Log(`----RowsAffected---join---joinMap[0]["username"]--`, joinMap[0]["username"])
		}
	}

	// join left 1
	{
		joinMap := make([]map[string]interface{}, 0)

		tx := db.Model(&z1db.MongoUserRole{}).Joins(
			"LEFT JOIN mongo_user ON mongo_user_role.uid = mongo_user.id",
		).Joins(
			"JOIN mongo_role ON mongo_user_role.rid = mongo_role.id",
		).Select(
			"mongo_user.username",
			"mongo_role.name",
			"mongo_user.id as id1",
			"mongo_role.id as id2",
			"role.des",
		).Find(&joinMap)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			b, err := json.MarshalIndent(joinMap, ``, `  `)
			z1err.Check(err)
			t.Log(`----RowsAffected---join---1-----`, string(b))
			t.Log(`----RowsAffected---join---RowsAffected--`, tx.RowsAffected)
		}
	}

	// join left 2
	{
		joinMap := make([]map[string]interface{}, 0)

		tx := db.Model(&z1db.MongoUserRole{}).Joins(
			"JOIN mongo_user ON mongo_user_role.uid = mongo_user.id",
		).Joins(
			"LEFT JOIN mongo_role ON mongo_user_role.rid = mongo_role.id",
		).Select(
			"mongo_user.username",
			"mongo_role.name",
			"mongo_user.id as id1",
			"mongo_role.id as id2",
			"role.des",
		).Find(&joinMap)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			b, err := json.MarshalIndent(joinMap, ``, `  `)
			z1err.Check(err)
			t.Log(`----RowsAffected---join---1-----`, string(b))
			t.Log(`----RowsAffected---join---RowsAffected--`, tx.RowsAffected)
		}
	}

	// join left 3
	{
		joinMap := make([]map[string]interface{}, 0)

		tx := db.Model(&z1db.MongoUserRole{}).Joins(
			"LEFT JOIN mongo_user ON mongo_user_role.uid = mongo_user.id",
		).Joins(
			"LEFT JOIN mongo_role ON mongo_user_role.rid = mongo_role.id",
		).Select(
			"mongo_user.username",
			"mongo_role.name",
			"mongo_user.id as id1",
			"mongo_role.id as id2",
			"role.des",
		).Find(&joinMap)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			b, err := json.MarshalIndent(joinMap, ``, `  `)
			z1err.Check(err)
			t.Log(`----RowsAffected---join---1-----`, string(b))
			t.Log(`----RowsAffected---join---RowsAffected--`, tx.RowsAffected)
		}
	}
}

func z1dbJoin(t *testing.T) {
	db := z1db.DB.Gorm

	// join
	{
		joinMap := make([]map[string]interface{}, 0)

		tx := db.Model(&z1db.UserRole{}).Joins(
			"JOIN user ON user_role.uid = user.id",
		).Joins(
			"JOIN role ON user_role.rid = role.id",
		).Select(
			"user.username",
			"role.name",
			"user.id as id1",
			"role.id as id2",
			"role.des",
		).Find(&joinMap)

		{
			// https://gorm.io/docs/sql_builder.html#DryRun-Mode
			// https://gorm.io/docs/session.html#DryRun
			txSql := db.Session(&gorm.Session{DryRun: true}).Model(&z1db.UserRole{}).Joins(
				"JOIN user ON user_role.uid = user.id",
			).Joins(
				"JOIN role ON user_role.rid = role.id",
			).Select(
				"user.username",
				"role.name",
				"user.id",
				"role.id",
			).Find(&joinMap)
			log.Println(`-------View the SQL that Gorm will execute---------`, db.Dialector.Explain(
				txSql.Statement.SQL.String(),
				txSql.Statement.Vars...,
			))
		}

		// tx := db.Model(&z1db.MongoUser{}).Select("username").Where("username like ?", `gorm_username%`).Count(&count)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			for i, _ := range joinMap {
				joinMap[i][`username`] = string(joinMap[i][`username`].([]uint8))
				joinMap[i][`name`] = string(joinMap[i][`name`].([]uint8))
			}
			b, err := json.MarshalIndent(joinMap, ``, `  `)
			z1err.Check(err)
			t.Log(`----RowsAffected---join---1-----`, string(b))
			t.Log(`----RowsAffected---join---joinMap[0]["username"]--`, joinMap[0]["username"])
		}
	}
}

func z1dbMongoFindCount(t *testing.T) {
	db := z1db.DB.Gorm

	// count
	{
		var count int64

		tx := db.Model(&z1db.MongoUser{}).Where("username like ?", `gorm_username%`).Count(&count)
		// tx := db.Model(&z1db.MongoUser{}).Select("username").Where("username like ?", `gorm_username%`).Count(&count)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select---count-----`, tx.RowsAffected)
			t.Log(`----RowsAffected---Select---count-----`, count)
		}
	}
}

func z1dbGormCreate(t *testing.T) {
	db := z1db.DB.Gorm

	// create
	{
		user := z1db.User{
			Username: `gorm_username`,
			Email:    `gorm_email@qq.com`,
		}
		tx := db.Model(&z1db.User{}).Create(&user)
		// tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--1---`, tx.RowsAffected)
		}

		user = z1db.User{
			Username: `gorm_username2`,
			Email:    `gorm_email@qq.com`,
		}
		tx = db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--2---`, tx.RowsAffected)
		}

		user = z1db.User{
			Username: `gorm_username3`,
			Email:    `gorm_email@qq.com`,
		}
		tx = db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--3---`, tx.RowsAffected)
		}

		user = z1db.User{
			Username: `gorm_username4`,
			Email:    `gorm_email@qq.com`,
		}
		tx = db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--4---`, tx.RowsAffected)
		}
	}

	// for join
	{
		users := []z1db.User{
			{
				Username: `gorm_username-join1`,
				Z1Model: z1db.Z1Model{
					ID: 1001,
				},
			},
			{
				Username: `gorm_username-join2`,
				Z1Model: z1db.Z1Model{
					ID: 1002,
				},
			},
			{
				Username: `gorm_username-join3`,
				Z1Model: z1db.Z1Model{
					ID: 1003,
				},
			},
		}
		user := z1db.User{}
		user.ID = 1007
		user.Username = `gorm_username-1007`
		tx := db.Model(&z1db.User{}).Create(&users)
		// tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--join--1---`, tx.RowsAffected)
		}

		roles := []z1db.Role{
			{
				Name: `role-join1`,
				Z1Model: z1db.Z1Model{
					ID: 1001,
				},
			},
			{
				Name: `role-join2`,
				Z1Model: z1db.Z1Model{
					ID: 1002,
				},
			},
			{
				Name: `role-join3`,
				Z1Model: z1db.Z1Model{
					ID: 1003,
				},
			},
		}
		tx = db.Model(&z1db.Role{}).Create(&roles)
		// tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--join--2---`, tx.RowsAffected)
		}

		userRoles := []z1db.UserRole{
			{
				Uid: 1001,
				Rid: 1001,
			},

			{
				Uid: 1002,
				Rid: 1002,
			},
			{
				Uid: 1002,
				Rid: 1003,
			},

			{
				Uid: 1003,
				Rid: 1002,
			},
			{
				Uid: 735,
				Rid: 1002,
			},
			{
				Uid: 1003,
				Rid: 735,
			},
		}
		tx = db.Model(&z1db.UserRole{}).Create(&userRoles)
		// tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--join--3---`, tx.RowsAffected)
		}

	}
}

func z1dbGormDelete(t *testing.T) {
	db := z1db.DB.Gorm

	// deleted
	{
		tx := db.Model(&z1db.User{}).Where("username = ?", `gorm_username2`).Delete(&z1db.User{})
		// tx := db.Where("username = ?", `gorm_username2`).Delete(&z1db.User{})

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--Delete-----`, tx.RowsAffected)
		}
	}
}

func z1dbGormUpdate(t *testing.T) {
	db := z1db.DB.Gorm

	// update
	{
		tx := db.Model(&z1db.User{}).Where(`username=?`, `gorm_username`).Updates(z1db.User{Username: `gorm_username-updated`})

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--update-----`, tx.RowsAffected)
		}
	}
}

func z1dbGormFindOne(t *testing.T) {
	db := z1db.DB.Gorm

	// select one
	{
		user := z1db.User{}
		tx := db.Model(&z1db.User{}).Where("username = ?", `gorm_username-updated`).Find(&user)
		// tx := db.Model(&z1db.User{}).Select("username").Where("username = ?", `gorm_username-updated`).Find(&user)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select----one-----`, tx.RowsAffected)
			t.Log(`----Result---Select-----one----`, user)
		}
	}
}

func z1dbGormFindMany(t *testing.T) {
	db := z1db.DB.Gorm

	// select
	{
		users := []z1db.User{}

		tx := db.Model(&z1db.User{}).Where("username like ?", `gorm_username%`).Offset(1).Limit(20).Find(&users)
		// tx := db.Model(&z1db.User{}).Select("username").Where("username like ?", `gorm_username-updated`).Offset(1).Limit(20).Find(&users)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select---many-----`, tx.RowsAffected)
			t.Log(`----Result---Select----many----`, users)
		}
	}
}

func z1dbGormFindCount(t *testing.T) {
	db := z1db.DB.Gorm

	// count
	{
		var count int64

		tx := db.Model(&z1db.User{}).Where("username like ?", `gorm_username%`).Count(&count)
		// tx := db.Model(&z1db.User{}).Select("username").Where("username like ?", `gorm_username%`).Count(&count)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select---count-----`, tx.RowsAffected)
			t.Log(`----RowsAffected---Select---count-----`, count)
		}
	}
}

func z1dbRs(t *testing.T) {
	log.Println(`-------------`)
	// return

	db := z1db.DB.Gorm
	db = db.WithContext(context.WithValue(
		context.Background(),
		`z1AddStatement`,
		map[string]interface{}{
			`status`: 1,
		},
	))

	if `create` == `create` {
		user := z1db.MongoUser{
			Username: `gorm_username`,
			Email:    `gorm_email@qq.com`,
			Z1Model: z1db.Z1Model{
				ID: 1,
			},
		}
		// tx := db.Model(&z1db.MongoUser{}).Create(&user)
		tx := db.Create(&user)
		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected--create--1---`, tx.RowsAffected)
		}
	}

	if `find` == `find` {
		testFind(db, t, 735)
		for i := 0; i < 5; i++ {
			go func(db *gorm.DB, i int) {
				testFind(db, t, i)
			}(db, i)
		}
	}

	time.Sleep(time.Second * 100)
}

func testFind(db *gorm.DB, t *testing.T, i int) {

	// t.Log(`---------------------`, i)

	if `gorm` != `` {
		user := map[string]interface{}{}
		tx := db.Table(`mongo_user`).Where("username = ?", `gorm_username`).Find(&user)

		// tx := db.Model(&z1db.MongoUser{}).Select("username").Where("username = ?", `gorm_username-updated`).Find(&user)

		if tx.Error != nil {
			t.Error(tx.Error)
		} else {
			t.Log(`----RowsAffected---Select----one-----`, i, tx.RowsAffected)
			t.Log(`----Result---Select-----one----`, i, user)
		}

	} else {

	}

	// time.Sleep(time.Second * 1)

}
