package test

import (
	"fmt"
	"testing"

	"gitee.com/myzero1/gotool/z1captcha"
)

func TestCaptcha_Generate_Verify(t *testing.T) {
	c := z1captcha.NewZ1captcha()
	id, b64s, a, err := c.Generate()

	if err != nil {
		t.Errorf("NoStoreCaptcha.Generate() error = %v", err)
		return
	}

	fmt.Println("TestCaptcha_Generate_Verify,id and answer are:", id, a)
	fmt.Println("b64s is:", b64s)

	if !c.Verify(id, a, true) {
		t.Error("false")
	}
}

func TestCaptcha_GenerateIdQuestionAnswer_Verify(t *testing.T) {
	c := z1captcha.NewZ1captcha(z1captcha.Param{Length: 4, IDLength: 8})
	id, content, answer := c.GenerateIdQuestionAnswer()

	fmt.Println("TestCaptcha_GenerateIdQuestionAnswer_Verify,id and answer are:", id, answer)

	if !c.Verify(id, content, true) {
		t.Error("false")
	}
}
