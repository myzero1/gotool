package test

import (
	"fmt"
	"testing"

	"gitee.com/myzero1/gotool/z1struct"
)

func TestStruck_GetDefault(t *testing.T) {
	fmt.Println("\n\n\n----------test split line,TestStruck_GetDefault-----------")
	type MySrc struct {
		MyInt    int     `z1Default:"735"`
		MyInt8   int8    `z1Default:"7"`
		MyUint   uint8   `z1Default:"8"`
		MyFloat  float64 `z1Default:"-12.34"`
		MyBool   bool    `z1Default:"true"`
		MyString string  `z1Default:"myzero1"`
	}

	dst := MySrc{}

	z1struct.GetDefault(&dst)

	t.Log(dst)
}

func TestStruck_FillDefault(t *testing.T) {
	fmt.Println("\n\n\n----------test split line,TestStruck_FillDefault-----------")
	type MySrc struct {
		MyInt    int     `z1Default:"735"`
		MyInt8   int8    `z1Default:"7"`
		MyUint   uint8   `z1Default:"8"`
		MyFloat  float64 `z1Default:"-12.34"`
		MyBool   bool    `z1Default:"true"`
		MyString string  `z1Default:"myzero1"`
	}

	dst := MySrc{MyString: "woogle735"}

	z1struct.FillDefault(&dst)

	t.Log(dst)
}

func TestStruck_Copy(t *testing.T) {
	fmt.Println("\n\n\n----------test split line,TestStruck_Copy-----------")
	type MySrc struct {
		ID   int
		Name string
	}

	src := MySrc{ID: 1, Name: "myzero1"}
	dst := MySrc{}

	z1struct.Copy(&dst, &src)

	t.Log(dst)
}

func TestStruck_Load(t *testing.T) {
	fmt.Println("\n\n\n----------test split line,TestStruck_Load-----------")

	type MySrc struct {
		ID   int
		Name string
		Aget int
	}
	type MyDes struct {
		ID   int
		Name string
		Aget int
	}

	src := MySrc{ID: 1, Name: "myzero1"}
	dst := MyDes{Aget: 20}

	z1struct.Load(&dst, src)

	t.Log(dst)
}
