package test

import (
	"encoding/base64"
	"fmt"
	"testing"

	"gitee.com/myzero1/gotool/z1crypto"
	"gitee.com/myzero1/gotool/z1err"
)

func TestAes(t *testing.T) {
	fmt.Println("\n\n\n----------test split line,TestAes-----------")
	orig := "myzero1"
	key := "1234567890123456"
	fmt.Println("原文：", orig)

	encryptCode := z1crypto.AesEncrypt(orig, key)
	fmt.Println("密文：", encryptCode)

	decryptCode := z1crypto.AesDecrypt(encryptCode, key)
	fmt.Println("解密结果：", decryptCode)
}

func TestRsaKeyPairs(t *testing.T) {
	fmt.Println("\n\n\n----------test split line,TestRsaKeyPairs-----------")
	privateKeyString, publicKeyString := z1crypto.RsaKeyPairs("key-pairs", 2048)
	println("privateKeyString \n")
	println(privateKeyString)
	println("publicKeyString \n")
	println(publicKeyString)
}

func TestRsa(t *testing.T) {
	fmt.Println("\n\n\n----------test split line,TestRsa-----------")

	// 私钥生成
	//openssl genrsa -out rsa_private_key.pem 1024
	//z1crypto.KeyPairs("key-pairs", "P521")
	var privateKey = []byte(`
-----BEGIN PRIVATE KEY-----
MIIEpAIBAAKCAQEAtO/zC2H85XMY5YRVcSANp+UjoCNaNC4jZ1zedp3iM92aPqP3
2SBRochBBDBaVdz9+2Z1aiv60IdSdSQ9410Sb2n794yOCBxBQcslTRxiHr4N7woQ
9hhp9NvJcZRZAWnLSWM+KXDeULlBGzOxKsEs0XWYcsBhdSlu19dAatrC8n40TWNr
EbWoOZpFSVOXMYv1YWiMqAyG7ZWwh4wVQMlkEbLaVmywkWc4qRuebvqUdQDM6bL8
9B4aNwWTY6Ku0l17QeFsr+FPPf33RGE6y7pFWF0p6qnlXvyqGlAt8prc0bUSGvki
/EuU9oQk5erBBGlTcxEFrkvz4NR5Lavw+/krOwIDAQABAoIBAEvReGC+sMBdGIOk
jkEXE2HVTLZf2f6fjspTSg0eX5koNOgmwUbEmjBWv5fHwzLTpcHgzzasMy+KZO0d
QKmzaHQzTxgOV73iSR4OyBSl3h/Pt/sAltboqA2ow6LKbPmAoWShJ30qBJ6C4ltJ
r+WgiO1Ef5smLjBwKjhbxGyCOsptjvToxKRMAeXRJcg6sG4O+o2iXn4yHZ0PjLtM
pP2NkyOgvtfUkJWecOpEutw6aARHxQFG3IwKpK6VDkEQwE98uul4XdWe7tYh6+10
HR3/gxBizHTz5+b0jqDv4+sReWo8m5EliNW4q4ZDHQ9iL0u59OnLMWvkfovzed0r
b+uePgECgYEA6bsV3l8Y1m5aufRp3vetx8rKfgcuLk5e4AmtoUuXyf7CmM96xyr2
zoQXmo/15GuQ2JE53j1Bmp8H3JhxC3DWvwda55eBLL4uO1Eks87xTxxNJGOJSRqI
/bO9GJBQ+1pb6x4ziRT7BX9AFhjKjbyA8oAlwYSwhSncroLSzSiOqbsCgYEAxi0u
RO1fSuzfRUrNtdUop6nGWHQBas61QnRF7JUPcI+0cRniJ1ouNHsPkFgnpIu5I3yD
W23guU3PK8PDpQ6DhliC++nG86ykoPpp+dVFyrOzuekKwHJ/hCcy7cGyAK3J0clv
UTL592Dn43MQXYKf3c9KGL/Se+RPTt2Vu19krIECgYEAm+QDNvjYeRTLgGGLR4KM
rsgL/LjPTWVKFEWAQCdKCxjXM9KWgkaerKkfkSBZkctIpZlfLmJO4lEVjkVCiK3v
HSRLtlGvf2ctMLTzNNZezjQDI7UbuN1w5z29bqe6y5q92fifyNtsfYMc/F/2oUhL
/PIJvKPClF6SaPaqoNlfJccCgYAWph89nWtgosW5o5zzBROKZerXPRrUXB93ItbC
/R4wnRvgOTcRjErvkIjs1EZjldxo49gXYO1oAC6HL+5yNMBvYDpIFCW6v6ZpB3WJ
Kgi56+PEgY/LAsr0DY7SgWqYjUQdh8oDG7JZxWd7JTFfnogqz4AGaBXkNumhd82f
Bn35AQKBgQCRyUUyy+JkLO9qzcir9OMktqfYteVHTwCsjAe6l16t3oPXWDBSz+95
ZDuyPaujYC2D1drj0+av6equgbti7rHu2oLXLhcaw5QesuJEUx9P55UURgu0S5m0
sVceXEIvOyp0RifOA43bOzwuxcbLAukTl+OaL8LQVZ+ZzV/9ja4kig==
-----END PRIVATE KEY-----
`)

	// 公钥: 根据私钥生成
	//openssl rsa -in rsa_private_key.pem -pubout -out rsa_public_key.pem
	var publicKey = []byte(`
-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtO/zC2H85XMY5YRVcSAN
p+UjoCNaNC4jZ1zedp3iM92aPqP32SBRochBBDBaVdz9+2Z1aiv60IdSdSQ9410S
b2n794yOCBxBQcslTRxiHr4N7woQ9hhp9NvJcZRZAWnLSWM+KXDeULlBGzOxKsEs
0XWYcsBhdSlu19dAatrC8n40TWNrEbWoOZpFSVOXMYv1YWiMqAyG7ZWwh4wVQMlk
EbLaVmywkWc4qRuebvqUdQDM6bL89B4aNwWTY6Ku0l17QeFsr+FPPf33RGE6y7pF
WF0p6qnlXvyqGlAt8prc0bUSGvki/EuU9oQk5erBBGlTcxEFrkvz4NR5Lavw+/kr
OwIDAQAB
-----END PUBLIC KEY-----
`)

	orig := "myzero1"
	cryted := z1crypto.RsaEncrypt(orig, publicKey)
	orig2 := z1crypto.RsaDecrypt(cryted, privateKey)
	fmt.Println("原文：", orig)
	fmt.Println("密文：", cryted)
	fmt.Println("解密结果：", orig2)
}

func TestMD5(t *testing.T) {
	fmt.Println("\n\n\n----------test split line,TestMD5-----------")
	orig := "myzero1"
	cryted := z1crypto.MD5Encode(orig)

	fmt.Println("原文：", orig)
	fmt.Println("密文：", cryted)
	fmt.Println("密文验证：", z1crypto.MD5Check(orig, cryted))
}

func TestBase64(t *testing.T) {
	fmt.Println("\n\n\n----------test split line,TestBase64-----------")

	orig := "my name is myzero1,what's your name?"
	cryted := base64.RawURLEncoding.EncodeToString([]byte(orig))
	orig2Data, err := base64.RawURLEncoding.DecodeString(cryted)
	z1err.Check(err)
	orig2 := string(orig2Data)

	fmt.Println("原文：", orig)
	fmt.Println("密文base64.RawURLEncoding.EncodeToString：", cryted)
	fmt.Println("密文base64.RawStdEncoding.EncodeToString：", base64.RawStdEncoding.EncodeToString([]byte(orig)))
	fmt.Println("解密结果：", orig2)

}
