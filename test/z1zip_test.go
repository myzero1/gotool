package test

import (
	"testing"

	"gitee.com/myzero1/gotool/z1err"
	"gitee.com/myzero1/gotool/z1zip"
)

func TestZ1zip(t *testing.T) {
	err := z1zip.Zip(`../tmp/t.zip`, `./z1info`, `../z1tryout/z1tryout.go`)
	z1err.Check(err)
	err = z1zip.UnZip(`../tmp/t.zip`, `../tmp/t`)
	z1err.Check(err)
	err = z1zip.ZipEncrypt(`../tmp/t`, `../tmp/t1.zip`, `123456`)
	z1err.Check(err)
	err = z1zip.UnZipDecrypt(`../tmp/t1.zip`, `../tmp/t1`, `123456`)
	z1err.Check(err)
}
