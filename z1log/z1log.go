package z1log

import (
	"bytes"
	"os"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

var Z1logger *zap.Logger
var Z1logBuffer *bytes.Buffer
var defaultCnf = lumberjack.Logger{
	// Filename:   "/tmp/foo.log", // 日志文件路径
	Filename:   os.TempDir() + "/z1log/z1log.log", // 日志文件路径
	MaxSize:    500,                               // 每个日志文件保存的最大尺寸 单位：M
	MaxBackups: 3,                                 // 日志文件最多保存多少个备份
	MaxAge:     28,                                // 文件最多保存多少天
	Compress:   true,                              // 是否压缩
}

// https://blog.csdn.net/test1280/article/details/117266333
var encoderConfig = zapcore.EncoderConfig{
	TimeKey:       "time",
	LevelKey:      "level",
	NameKey:       "name",
	CallerKey:     "line",
	MessageKey:    "msg",
	FunctionKey:   "func",
	StacktraceKey: "stacktrace",
	LineEnding:    zapcore.DefaultLineEnding,
	EncodeLevel:   zapcore.LowercaseLevelEncoder,
	EncodeTime:    zapcore.TimeEncoderOfLayout("2006-01-02 15:04:05.0000000"),
	// EncodeTime:     zapcore.EpochTimeEncoder, // 时间戳
	EncodeDuration: zapcore.SecondsDurationEncoder,
	EncodeCaller:   zapcore.FullCallerEncoder,
	EncodeName:     zapcore.FullNameEncoder,
}

func init() {
	DefultLog()
}

// SetLog(`/tmp/mylog/mylog.log`,400,3,28,0,1)
// SetLog(Filename string,istimestrap,MaxSize,MaxBackups,MaxAge,Compress,AddLogBuffer int)
func SetLog(filename string, options ...int) {
	encoderCnf := encoderConfig

	if filename == "" {
		if len(options) > 5 && options[5] == 1 {
			Z1logBuffer = new(bytes.Buffer)
			resetLog(
				zapcore.NewJSONEncoder(encoderCnf),
				zapcore.NewMultiWriteSyncer(
					zapcore.AddSync(Z1logBuffer),
					zapcore.AddSync(os.Stdout),
				),
				zap.DebugLevel,
			)
		} else {
			resetLog(
				zapcore.NewJSONEncoder(encoderCnf),
				zapcore.NewMultiWriteSyncer(
					zapcore.AddSync(os.Stdout),
				),
				zap.DebugLevel,
			)
		}
	} else {
		cnf := defaultCnf
		cnf.Filename = filename

		{
			if len(options) > 0 && options[0] == 1 {
				encoderCnf.EncodeTime = zapcore.EpochTimeEncoder
			}

			if len(options) > 1 {
				cnf.MaxSize = options[1]
			}

			if len(options) > 2 {
				cnf.MaxBackups = options[2]
			}

			if len(options) > 3 {
				cnf.MaxAge = options[3]
			}

			if len(options) > 4 {
				if options[4] == 0 {
					cnf.Compress = false
				} else {
					cnf.Compress = true
				}
			}
		}

		// fileWriter := zapcore.AddSync(&cnf) // 这样会有问题，这个具体咋回事没搞清楚
		fileWriter := zapcore.AddSync(&lumberjack.Logger{
			Filename:   cnf.Filename,
			MaxSize:    cnf.MaxSize,
			MaxBackups: cnf.MaxBackups,
			MaxAge:     cnf.MaxAge,
			Compress:   cnf.Compress,
		})

		if len(options) > 5 && options[5] == 1 {
			Z1logBuffer = new(bytes.Buffer)
			resetLog(
				zapcore.NewJSONEncoder(encoderCnf),
				zapcore.NewMultiWriteSyncer(
					fileWriter,
					zapcore.AddSync(Z1logBuffer),
					zapcore.AddSync(os.Stdout),
				),
				zap.DebugLevel,
			)
		} else {
			resetLog(
				zapcore.NewJSONEncoder(encoderCnf),
				zapcore.NewMultiWriteSyncer(
					fileWriter,
					zapcore.AddSync(os.Stdout),
				),
				zap.DebugLevel,
			)
		}
	}
}

func DefultLog() {
	// https://www.liuvv.com/p/5d303099.html
	// lumberjack.Logger is already safe for concurrent use, so we don't need to
	// lock it.

	resetLog(
		// zapcore.NewJSONEncoder(zap.NewProductionEncoderConfig()),                              // 编码器配置
		zapcore.NewJSONEncoder(encoderConfig),                   // 编码器配置
		zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout)), // 打印到控制台和文件
		zap.DebugLevel, // 日志级别
	)

	// Z1logger.Info("failed to fetch URL",
	// 	zap.Int("attempt", 3))

	// Z1sugar.Infof("Failed to fetch URL: %s", `url`)
	// Z1sugar.Infow("failed to fetch URL",
	// 	"attempt", 3,
	// )
}

func resetLog(enc zapcore.Encoder, ws zapcore.WriteSyncer, enab zapcore.LevelEnabler) {
	core := zapcore.NewCore(enc, ws, enab)

	Z1logger = &zap.Logger{}
	Z1logger = zap.New(core)
}

func Debug(msg string, fields ...zapcore.Field) {
	Z1logger.Debug(msg, fields...)
}
func Debugln(args ...interface{}) {
	Z1logger.Sugar().Debug(args...)
}
func Debugw(msg string, keysAndValues ...interface{}) {
	Z1logger.Sugar().Debugw(msg, keysAndValues...)
}
func Debugf(template string, args ...interface{}) {
	Z1logger.Sugar().Debugf(template, args...)
}

func Info(msg string, fields ...zapcore.Field) {
	Z1logger.Info(msg, fields...)
}
func Infoln(args ...interface{}) {
	Z1logger.Sugar().Info(args...)
}
func Infow(msg string, keysAndValues ...interface{}) {
	Z1logger.Sugar().Infow(msg, keysAndValues...)
}
func Infof(template string, args ...interface{}) {
	Z1logger.Sugar().Infof(template, args...)
}

func Warn(msg string, fields ...zapcore.Field) {
	Z1logger.Warn(msg, fields...)
}
func Warnln(args ...interface{}) {
	Z1logger.Sugar().Warn(args...)
}
func Warnw(msg string, keysAndValues ...interface{}) {
	Z1logger.Sugar().Warnw(msg, keysAndValues...)
}
func Warnf(template string, args ...interface{}) {
	Z1logger.Sugar().Warnf(template, args...)
}

func Error(msg string, fields ...zapcore.Field) {
	Z1logger.Error(msg, fields...)
}
func Errorln(args ...interface{}) {
	Z1logger.Sugar().Error(args...)
}
func Errorw(msg string, keysAndValues ...interface{}) {
	Z1logger.Sugar().Errorw(msg, keysAndValues...)
}
func Errorf(template string, args ...interface{}) {
	Z1logger.Sugar().Errorf(template, args...)
}

func DPanic(msg string, fields ...zapcore.Field) {
	Z1logger.DPanic(msg, fields...)
}
func DPanicln(args ...interface{}) {
	Z1logger.Sugar().DPanic(args...)
}
func DPanicw(msg string, keysAndValues ...interface{}) {
	Z1logger.Sugar().DPanicw(msg, keysAndValues...)
}
func DPanicf(template string, args ...interface{}) {
	Z1logger.Sugar().DPanicf(template, args...)
}

func Panic(msg string, fields ...zapcore.Field) {
	Z1logger.Panic(msg, fields...)
}
func Panicln(args ...interface{}) {
	Z1logger.Sugar().Panic(args...)
}
func Panicw(msg string, keysAndValues ...interface{}) {
	Z1logger.Sugar().Panicw(msg, keysAndValues...)
}
func Panicf(template string, args ...interface{}) {
	Z1logger.Sugar().Panicf(template, args...)
}

func Fatal(msg string, fields ...zapcore.Field) {
	Z1logger.Fatal(msg, fields...)
}
func Fatalln(args ...interface{}) {
	Z1logger.Sugar().Fatal(args...)
}
func Fatalw(msg string, keysAndValues ...interface{}) {
	Z1logger.Sugar().Fatalw(msg, keysAndValues...)
}
func Fatalf(template string, args ...interface{}) {
	Z1logger.Sugar().Fatalf(template, args...)
}
