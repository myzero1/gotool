package z1cnf

import (
	"bytes"
	"io/ioutil"
	"os"
	"path/filepath"

	"gitee.com/myzero1/gotool/z1err"
	"gitee.com/myzero1/gotool/z1log"
	"github.com/spf13/viper"
	"go.uber.org/zap"
)

var Z1viper *viper.Viper

const (
	YAML int8 = iota
	JSON
)

func init() {
	Z1viper = viper.New()
}

func Init(contentType int8, fileName, content string) {
	// Init(z1cnf.JSON, `config/config.yaml`, `{"contentType":"json","name":"张三","年龄":18}`)

	// 	Init(YAML, `config/config.yaml`, `
	// 成绩:
	//   张三: 90
	//   李四: 95
	// 班级: 一年级
	// 老师:  # 数学老师和语文老师
	// - 张老师
	// - 李老师`)

	Z1viper.SetConfigFile(fileName)
	Z1viper.SetConfigType("yaml")
	Z1viper.WatchConfig()

	_, err := os.Lstat(fileName)

	if os.IsNotExist(err) {
		dir := filepath.Dir(fileName)

		os.MkdirAll(dir, 0666)

		f, err := os.Create(fileName)
		z1err.Check(err)
		if contentType == JSON {
			{
				// j := []byte(content)
				// // j := []byte(`{"name": "John", "age": 30}`)
				// y, err := yaml.JSONToYAML(j)
				// // j2, err := yaml.YAMLToJSON(y)
				// z1err.Check(err)
				// f.WriteString(string(y))
			}

			{
				content2 := Convert(`json`, content, `yaml`)
				f.WriteString(content2)
			}
		} else {
			f.WriteString(content)
		}
		f.Close()
		z1err.Check(Z1viper.ReadInConfig())
		z1log.Info(`"z1cnf" is initialized successfully. Please adjust the configuration file and run again.`, zap.String(`fileName`, fileName))
	} else {
		z1err.Check(Z1viper.ReadInConfig())
		z1log.Info(`"z1cnf" has been initialized. If you need to re initialize, please delete the configuration file first.`, zap.String(`fileName`, fileName))
	}

}

func Convert(sourceType, sourceContent, targetType string) (targetContent string) {
	sviper := viper.New()
	sviper.SetConfigType(sourceType)
	j := []byte(sourceContent)
	z1err.Check(sviper.ReadConfig(bytes.NewBuffer(j)))

	tviper := viper.New()
	tviper.SetConfigType(targetType)
	tviper.Set(`data`, sviper.AllSettings())

	subViper := tviper.Sub(`data`)
	tmpfile := os.TempDir() + `z1cnfConvert.` + targetType
	z1err.Check(subViper.WriteConfigAs(tmpfile))

	b, err := ioutil.ReadFile(tmpfile)
	z1err.Check(err)
	targetContent = string(b)

	return
}
