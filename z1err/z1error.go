package z1err

import (
	"fmt"
	"strings"

	pkgerrors "github.com/pkg/errors"
)

// https://www.cnblogs.com/zhangboyu/p/7911190.html

// Handle catching error to return or assign to recoverFunc.
// eg1:defer z1err.Handle(&err)
// eg2:defer z1err.Handle(nil, func(err error) {...}
func Handle(returnErr *error, recoverFunc ...func(err error)) {
	r := recover()
	if nil != r {
		errMsg := fmt.Sprintf(`%s`, r)
		err := pkgerrors.New(errMsg)
		if returnErr != nil {
			*returnErr = err
		} else {
			if len(recoverFunc) > 0 {
				recoverFunc[0](err)
			} else {
				panic("z1error.Handle has not this logic")
			}
		}
	}
}

// Check check and add addon msg to error or panic.
// eg1:Check(err)
// eg2:Check(err, true)
// eg3:Check(err, false)
// eg4:Check(err, true, "this is addon msg")
// eg5:Check(err, false, "this is addon msg")
func Check(e error, opt ...interface{}) (noerr bool, wrapedErr error) {
	if e != nil {
		noerr = false

		if 0 == len(opt) {
			//只附加新的信息
			// func WithMessage(err error, message string) error
			//只附加调用堆栈信息
			wrapedErr = pkgerrors.WithStack(e)
			panic(wrapedErr)
		} else {
			msg := ``
			if len(opt) > 1 {
				myMsg, ok := opt[1].(string)
				if ok {
					msg = myMsg
				}
			}

			//同时附加堆栈和信息
			wrapedErr = pkgerrors.Wrap(e, msg)

			if len(opt) > 0 {
				isPanic, ok := opt[0].(bool)
				if ok && isPanic {
					panic(wrapedErr)
				}
			}
		}
	} else {
		noerr = true
	}

	return
}

// StackPrint print msg with stack and can set skip.
// eg:z1err.StackPrint(err)
// eg:z1err.StackPrint(err, 7)
func StackPrint(err error, skip ...int) (str string) {
	// https://studygolang.com/articles/17430?fr=sidebar
	skipFlag := 7
	if len(skip) > 0 {
		skipFlag = skip[0]
	}
	errCause := fmt.Sprintf(`%v`, pkgerrors.Cause(err))
	errInfo := fmt.Sprintf(`%+v`, err)
	errInfo2 := strings.Replace(errInfo, errCause, "", -1)
	errInfoArr := strings.Split(errInfo2, "\n")
	if len(errInfoArr) > skipFlag {
		str = errCause + "\n" + strings.Join(errInfoArr[skipFlag:], "\n")
	} else {
		str = errCause
	}

	return
}
