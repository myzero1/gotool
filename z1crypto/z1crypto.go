package z1crypto

import (
	"bytes"
	"crypto/aes"
	"crypto/cipher"
	"crypto/md5"
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/hex"
	"encoding/pem"
	"errors"
	"os"

	"gitee.com/myzero1/gotool/z1err"
)

// https://mojotv.cn/go/golang-crypto
// https://blog.csdn.net/itpika/article/details/101283640

// AesEncrypt("woolge735", "1234567890123456")
// key's length must in 16/24/32
func AesEncrypt(orig, key string) (cryted string) {
	// 转成字节数组
	origData := []byte(orig)
	k := []byte(key)

	// 分组秘钥
	block, err := aes.NewCipher(k)
	z1err.Check(err, true, "key 长度必须 16/24/32")

	// 获取秘钥块的长度
	blockSize := block.BlockSize()
	// 补全码
	origData = PKCS7Padding(origData, blockSize)
	// 加密模式
	blockMode := cipher.NewCBCEncrypter(block, k[:blockSize])
	// 创建数组
	crytedData := make([]byte, len(origData))
	// 加密
	blockMode.CryptBlocks(crytedData, origData)
	//使用RawURLEncoding 不要使用StdEncoding
	//不要使用StdEncoding  放在url参数中回导致错误
	cryted = base64.RawURLEncoding.EncodeToString(crytedData)

	return
}

// AesEncrypt("Y14T6rcetYC2WoB2y5JGNQ", "1234567890123456")
// key's length must in 16/24/32
func AesDecrypt(cryted, key string) (orig string) {
	//使用RawURLEncoding 不要使用StdEncoding
	//不要使用StdEncoding  放在url参数中回导致错误
	crytedByte, _ := base64.RawURLEncoding.DecodeString(cryted)
	k := []byte(key)

	// 分组秘钥
	block, err := aes.NewCipher(k)
	z1err.Check(err, true, "key 长度必须 16/24/32")
	// 获取秘钥块的长度
	blockSize := block.BlockSize()
	// 加密模式
	blockMode := cipher.NewCBCDecrypter(block, k[:blockSize])
	// 创建数组
	origData := make([]byte, len(crytedByte))
	// 解密
	blockMode.CryptBlocks(origData, crytedByte)
	// 去补全码
	origData = PKCS7UnPadding(origData)
	orig = string(origData)

	return
}

// RsaKeyPairs("keyName", 2048)
func RsaKeyPairs(keyName string, bits int) (privateKeyString, publicKeyString string) {
	// 生成私匙，提供一个随机数和私匙的长度，目前主流的长度为1024、2048、3072、4096，
	// 但1024已经不在推荐使用了。

	privateKey, err := rsa.GenerateKey(rand.Reader, bits)
	z1err.Check(err)

	x509Encoded := x509.MarshalPKCS1PrivateKey(privateKey)
	privateBs := pem.EncodeToMemory(&pem.Block{Type: "PRIVATE KEY", Bytes: x509Encoded})
	privateKeyString = string(privateBs)
	privateFile, err := os.Create(keyName + ".private.pem")
	z1err.Check(err)

	_, err = privateFile.Write(privateBs)
	z1err.Check(err)

	x509EncodedPub, _ := x509.MarshalPKIXPublicKey(privateKey.Public())
	publicBs := pem.EncodeToMemory(&pem.Block{Type: "PUBLIC KEY", Bytes: x509EncodedPub})
	publicKeyString = string(publicBs)
	publicKeyFile, err := os.Create(keyName + ".public.pem")
	z1err.Check(err)

	_, err = publicKeyFile.Write(publicBs)
	z1err.Check(err)

	return
}

// 加密
func RsaEncrypt(orig string, publicKey []byte) (cryted string) {
	//解密pem格式的公钥
	block, _ := pem.Decode(publicKey)
	if block == nil {
		err := errors.New("public key error")
		z1err.Check(err)
	}
	// 解析公钥
	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	z1err.Check(err)
	// 类型断言
	pub := pubInterface.(*rsa.PublicKey)
	//加密
	ciphertext, err := rsa.EncryptPKCS1v15(rand.Reader, pub, []byte(orig))
	z1err.Check(err)

	cryted = base64.RawURLEncoding.EncodeToString(ciphertext)

	return
}

// 解密
func RsaDecrypt(cryted string, privateKey []byte) (orig string) {
	ciphertext, err := base64.RawURLEncoding.DecodeString(cryted)
	z1err.Check(err)
	//解密
	block, _ := pem.Decode(privateKey)
	if block == nil {
		err := errors.New("private key error!")
		z1err.Check(err)
	}
	//解析PKCS1格式的私钥
	priv, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	z1err.Check(err)
	// 解密
	origData, err := rsa.DecryptPKCS1v15(rand.Reader, priv, ciphertext)
	z1err.Check(err)

	orig = string(origData)

	return
}

// MD5Check("myzero1")
func MD5Check(content, encrypted string) bool {
	return MD5Encode(content) == encrypted
}

// MD5Encode("myzero1")
func MD5Encode(data string) string {
	h := md5.New()
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}

// -------------------unit func--------------

//补码
func PKCS7Padding(ciphertext []byte, blocksize int) []byte {
	padding := blocksize - len(ciphertext)%blocksize
	padtext := bytes.Repeat([]byte{byte(padding)}, padding)
	return append(ciphertext, padtext...)
}

//去码
func PKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}
