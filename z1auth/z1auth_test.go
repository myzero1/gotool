package z1auth_test

import (
	"flag"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitee.com/myzero1/gotool/z1auth"
	"gitee.com/myzero1/gotool/z1err"
)

func TestMain(m *testing.M) {
	defer pkgSetUp(`package z1auth_test`)()
	m.Run()
	os.Exit(0)
}

func TestZ1auth(t *testing.T) {
	t.Run(`gen-sn`, testGenSn)
	t.Run(`test-isVirtualMachine`, func(t *testing.T) {
		t.Cleanup(setUp(t.Name()))
		tmp, err := z1auth.IsVirtualMachine()
		t.Log(`isVirtualMachine`, tmp, err)
	})
	t.Run(`test-getCPUID`, func(t *testing.T) {
		t.Cleanup(setUp(t.Name()))
		tmp, err := z1auth.GetCPUID()
		t.Log(`getCPUID`, tmp, err)
	})
	t.Run(`test-getBaseBoardID`, func(t *testing.T) {
		t.Cleanup(setUp(t.Name()))
		tmp, err := z1auth.GetBaseBoardID()
		t.Log(`getBaseBoardID`, tmp, err)
	})
}

func testGenSn(t *testing.T) {
	t.Cleanup(setUpGenSn(t.Name()))

	goldenFileName := filepath.Join(`testdata`, `TestZ1auth--gen-sn.golden`)
	golden, err := ioutil.ReadFile(goldenFileName)
	z1err.Check(err)
	goldenStr := string(golden)
	goldenInfo := strings.Split(goldenStr, "\n")
	for i, v := range goldenInfo {
		r := strings.Split(v, `;`)
		z1auth.Cnf(
			z1auth.WithSoft(r[0]),
			z1auth.WithVer(r[1]),
			z1auth.WithKey(r[2]),
		)

		sn := z1auth.GenSn()

		if sn != r[3] {
			t.Errorf(
				`[table offset:  %v] want %v,but GenSn(%v,%v,%v) = %v`,
				i,
				r[3],
				r[0],
				r[1],
				r[2],
				sn,
			)
		}

	}
	/*
		caseData := []struct {
			soft string
			ver  string
			key  string
			ret  string
		}{
			{
				"soft",
				"ver",
				"key",
				"7010e2d6e5cc642604ec3f69f24f42d9",
			},
			{
				"soft",
				"v1.2.3",
				"key2",
				"7010e2d6e5cc642604ec3f69f24f42d9",
			},
			{
				"soft",
				"v1.2.4",
				"key2",
				"7010e2d6e5cc642604ec3f69f24f42d9",
			},
		}

		for i, tt := range caseData {
			z1auth.Cnf(
				z1auth.WithSoft(tt.soft),
				z1auth.WithVer(tt.ver),
				z1auth.WithKey(tt.key),
			)

			sn := z1auth.GenSn()

			if sn != tt.ret {
				t.Errorf(
					`[table offset:  %v] want %v,but GenSn(%v,%v,%v) = %v`,
					i,
					tt.ret,
					tt.soft,
					tt.ver,
					tt.key,
					sn,
				)
			}

		}
	*/
}

// test fixture

func pkgSetUp(pkgName string) func() {
	log.Printf("\tpackage SetUp fixture for %s\n", pkgName)

	{
		content, err := ioutil.ReadFile(filepath.Join("testdata", ".gitignore"))
		z1err.Check(err)
		log.Println(`Test testdata file`)
		log.Println(string(content))
	}

	return func() {
		log.Printf("\tpackage TearDown fixture for %s\n", pkgName)
	}
}

func setUp(testName string) func() {
	log.Printf("\tsetUp fixture for %s\n", testName)

	return func() {
		log.Printf("\ttearDown fixture for %s\n", testName)
	}
}

var update = flag.Bool(`update`, false, `update .golden files`)

func setUpGenSn(testName string) func() {
	log.Printf("\tsetUp fixture for %s\n", testName)

	if *update {
		snInfojJson := []string{
			// `"soft";"ver";"key";"sn";"urlPre"`,
			`soft;ver;key;sn;urlPre`,
			`invoice2excel;v1.0.0;key1;sn;urlPre`,
			`invoice2excel;v1.0.1;key1;sn;urlPre`,
		}

		var genSnGolden []string
		for _, item := range snInfojJson {
			itemInfo := strings.Split(item, `;`)
			z1auth.Cnf(
				z1auth.WithSoft(itemInfo[0]),
				z1auth.WithVer(itemInfo[1]),
				z1auth.WithKey(itemInfo[2]),
			)
			sn := z1auth.GenSn()
			itemInfo[3] = sn
			genSnGolden = append(genSnGolden, strings.Join(itemInfo, `;`))

		}

		genSnGolden = append(genSnGolden, `soft;ver;key;sn;urlPre`)

		goldenFileName := filepath.Join(`testdata`, strings.ReplaceAll(testName, `/`, `--`)+`.golden`)
		err := ioutil.WriteFile(goldenFileName, []byte(strings.Join(genSnGolden, "\n")), 0644)
		z1err.Check(err)
	}

	return func() {
		log.Printf("\ttearDown fixture for %s\n", testName)
	}
}
