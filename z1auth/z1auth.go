package z1auth

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
	"runtime"
	"strings"
	"time"

	"gitee.com/myzero1/gotool/z1crypto"
	"gitee.com/myzero1/gotool/z1err"
	"golang.org/x/sys/windows/registry"
)

type auth struct {
	deviceID string
	soft     string
	ver      string

	key string
	sn  string

	urlPre string

	mustVm bool
}

type Option func(*auth)

var _auth *auth

func init() {
	_auth = &auth{}

	cpuID, err := getCPUID()
	z1err.Check(err)
	boardID, err := getBaseBoardID()
	z1err.Check(err)

	_auth.deviceID = z1crypto.MD5Encode(cpuID + boardID)
}

func Cnf(options ...Option) {
	for _, option := range options {
		option(_auth)
	}

	if _auth.mustVm {
		retIsVM, retErr := isVirtualMachine()
		z1err.Check(retErr)

		if retIsVM {
			panic(`Cannot run on virtual machine!`)
		}

	}

	if _auth.soft == `` || _auth.ver == `` {
		panic(`Soft and ver cannot be empty!`)
	}

	if _auth.key == `` && _auth.urlPre == `` {
		panic(`At least one of key and urlPre cannot be empty!`)
	}

	if _auth.key != `` {
		_auth.sn = GenSn()
	}
}

func Check() (ret bool) {
	if CheckSn() {
		return true
	}

	if Tryout() {
		return true
	}

	return CheckInputSn()
}

func WithDeviceID(deviceID string) Option {
	return func(a *auth) {
		a.deviceID = deviceID
	}
}

func WithSoft(soft string) Option {
	return func(a *auth) {
		a.soft = soft
	}
}

func WithVer(ver string) Option {
	return func(a *auth) {
		a.ver = ver
	}
}

func WithKey(key string) Option {
	return func(a *auth) {
		a.key = key
	}
}

func WithUrlPre(urlPre string) Option {
	return func(a *auth) {
		a.urlPre = urlPre
	}
}

func WithMustVm(mustVm bool) Option {
	return func(a *auth) {
		a.mustVm = mustVm
	}
}

func GenSn() (sn string) {
	snStr := _auth.soft + _auth.ver + _auth.key + _auth.deviceID

	sn = z1crypto.MD5Encode(snStr)

	return
}

func GetDeviceID() (deviceID string) {
	return _auth.deviceID
}

func GetInfo() (info string) {
	return fmt.Sprintf(
		"Info is {cupid:%s, soft:%s, ver:%s},please enter the sn: ",
		_auth.deviceID,
		_auth.soft,
		_auth.ver,
	)
}

func CheckSn() (ret bool) {
	// https://blog.csdn.net/weixin_44495599/article/details/120610347
	keyStr := _auth.soft + `\` + _auth.ver
	key, _, _ := registry.CreateKey(registry.CURRENT_USER, keyStr, registry.ALL_ACCESS)
	defer key.Close()

	snOld := ``
	snOld, _, _ = key.GetStringValue(`sn`)

	return _auth.sn == snOld
}

func Tryout() (ret bool) {
	// https://gitee.com/myzero1/tryout/raw/master/soft/ver/BFEBFBFF000906EA
	url := fmt.Sprintf(`%s/%s/%s/%s`, _auth.urlPre, _auth.soft, _auth.ver, _auth.deviceID)
	resp, err := http.Get(url)
	z1err.Check(err)

	if resp.StatusCode != 200 {
		return false
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	z1err.Check(err)
	bodystr := string(body)
	bodystr = strings.Trim(bodystr, ` `)
	deadline, err := time.ParseInLocation(`2006-01-02 15:04:05`, bodystr, time.Local)
	z1err.Check(err)

	remoteDate := resp.Header["Date"][0]
	remoteDate = strings.Split(remoteDate, `, `)[1]
	remoteTime, err := time.ParseInLocation(`02 Jan 2006 15:04:05 GMT`, remoteDate, time.Local)
	remoteTime = remoteTime.Add(8 * time.Hour)
	z1err.Check(err)

	if remoteTime.Unix() <= deadline.Unix() {
		return true
	}

	return
}

func CheckInputSn() (ret bool) {
	for {
		var sn string
		log.Printf(
			"Info is {cupid:%s, soft:%s, ver:%s},please enter the sn: ",
			_auth.deviceID,
			_auth.soft,
			_auth.ver,
		)
		fmt.Scan(&sn)

		if sn == _auth.sn {
			setSn()
			return true
		} else {
			return false
		}
	}
}

func setSn() {
	// https://blog.csdn.net/weixin_44495599/article/details/120610347
	keyStr := _auth.soft + `\` + _auth.ver
	key, _, _ := registry.CreateKey(registry.CURRENT_USER, keyStr, registry.ALL_ACCESS)
	defer key.Close()

	err := key.SetStringValue(`sn`, _auth.sn)
	z1err.Check(err)
}

func isVirtualMachine() (retIsVM bool, retErr error) {
	defer z1err.Handle(&retErr)

	// https://www.dazhuanlan.com/fangzhou422/topics/1681308
	model := ""
	var cmd *exec.Cmd

	if runtime.GOOS == "windows" {
		cmd = exec.Command("cmd", "/C", "wmic path Win32_ComputerSystem get Model")
	} else { // linux
		cmd = exec.Command("/bin/bash", "-c", "dmidecode | egrep -i 'system-product-name|product|domU'")
	}

	stdout, err := cmd.Output()
	z1err.Check(err)

	model = string(stdout)

	if strings.Contains(model, "VirtualBox") || strings.Contains(model, "Virtual Machine") || strings.Contains(model, "VMware Virtual Platform") ||
		strings.Contains(model, "KVM") || strings.Contains(model, "Bochs") || strings.Contains(model, "HVM domU") {
		retIsVM = true
	}

	return
}

// GetCPUID 获取cpuid
func getCPUID() (retCpuid string, retErr error) {
	defer z1err.Handle(&retErr)

	// https://www.cnblogs.com/wuyaxiansheng/p/13576174.html
	cmd := exec.Command("wmic", "cpu", "get", "processorid")
	b, err := cmd.CombinedOutput()
	z1err.Check(err)

	cpuid := string(b)
	cpuid = cpuid[12 : len(cpuid)-2]
	cpuid = strings.ReplaceAll(cpuid, "\n", "")
	cpuid = strings.ReplaceAll(cpuid, "\r", "")
	cpuid = strings.ReplaceAll(cpuid, " ", "")

	retCpuid = cpuid

	return
}

// GetBaseBoardID 获取主板的id
func getBaseBoardID() (retBoardID string, retErr error) {
	defer z1err.Handle(&retErr)

	cmd := exec.Command("wmic", "baseboard", "get", "serialnumber")
	b, err := cmd.CombinedOutput()
	z1err.Check(err)

	cpuid := string(b)
	cpuid = cpuid[12 : len(cpuid)-2]
	cpuid = strings.ReplaceAll(cpuid, "\n", "")
	retBoardID = cpuid

	return
}
