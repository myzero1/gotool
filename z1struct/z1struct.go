package z1struct

import (
	"errors"
	"fmt"
	"reflect"
	"strconv"

	"gitee.com/myzero1/gotool/z1err"
)

// GetDefault Get default vlue for struck,for tag.
func GetDefault(dst interface{}) {
	dstV := reflect.Indirect(reflect.ValueOf(dst))
	dstT := dstV.Type()

	if !dstV.CanAddr() {
		z1err.Check(errors.New("copy to value is unaddressable"))
	}

	ints := map[string]string{
		"int8":  "int8",
		"int16": "int16",
		"int32": "int32",
		"int64": "int64",
		"int":   "int",
	}
	uints := map[string]string{
		"uint8":  "uint8",
		"uint16": "uint16",
		"uint32": "uint32",
		"uint64": "uint64",
		"uint":   "uint",
	}
	floats := map[string]string{
		"float":   "float",
		"float64": "float64",
	}

	for i := 0; i < dstT.NumField(); i++ {
		defaultVal := dstT.Field(i).Tag.Get("z1Default")
		typeTmp := fmt.Sprintf(`%s`, dstT.Field(i).Type)
		_, intOk := ints[typeTmp]
		_, uintOk := uints[typeTmp]
		_, floatOk := floats[typeTmp]

		switch {
		case "string" == typeTmp:
			dstV.Field(i).SetString(defaultVal)
		case "bool" == typeTmp:
			tmpVal, _ := strconv.ParseBool(defaultVal)
			dstV.Field(i).SetBool(tmpVal)
		case intOk:
			tmpVal, _ := strconv.ParseInt(defaultVal, 10, 64)
			dstV.Field(i).SetInt(tmpVal)
		case uintOk:
			tmpVal, _ := strconv.ParseUint(defaultVal, 10, 64)
			dstV.Field(i).SetUint(tmpVal)
		case floatOk:
			tmpVal, _ := strconv.ParseFloat(defaultVal, 64)
			dstV.Field(i).SetFloat(tmpVal)
		}
	}
}

// FillDefault Fill nill field width default vlue for struck,for tag.
func FillDefault(dst interface{}) {
	dstV := reflect.Indirect(reflect.ValueOf(dst))
	dstT := dstV.Type()

	if !dstV.CanAddr() {
		z1err.Check(errors.New("copy to value is unaddressable"))
	}

	ints := map[string]string{
		"int8":  "int8",
		"int16": "int16",
		"int32": "int32",
		"int64": "int64",
		"int":   "int",
	}
	uints := map[string]string{
		"uint8":  "uint8",
		"uint16": "uint16",
		"uint32": "uint32",
		"uint64": "uint64",
		"uint":   "uint",
	}
	floats := map[string]string{
		"float":   "float",
		"float64": "float64",
	}

	for i := 0; i < dstT.NumField(); i++ {
		defaultVal := dstT.Field(i).Tag.Get("z1Default")
		typeTmp := fmt.Sprintf(`%s`, dstT.Field(i).Type)
		valFiledTmp := dstV.Field(i)
		_, intOk := ints[typeTmp]
		_, uintOk := uints[typeTmp]
		_, floatOk := floats[typeTmp]

		switch {
		case "string" == typeTmp:
			if isZeroOfUnderlyingType(valFiledTmp.Interface()) {
				valFiledTmp.SetString(defaultVal)
			}
		case "bool" == typeTmp:
			if isZeroOfUnderlyingType(valFiledTmp.Interface()) {
				tmpVal, _ := strconv.ParseBool(defaultVal)
				valFiledTmp.SetBool(tmpVal)
			}
		case intOk:
			if isZeroOfUnderlyingType(valFiledTmp.Interface()) {
				tmpVal, _ := strconv.ParseInt(defaultVal, 10, 64)
				valFiledTmp.SetInt(tmpVal)
			}
		case uintOk:
			if isZeroOfUnderlyingType(valFiledTmp.Interface()) {
				tmpVal, _ := strconv.ParseUint(defaultVal, 10, 64)
				valFiledTmp.SetUint(tmpVal)
			}
		case floatOk:
			if isZeroOfUnderlyingType(valFiledTmp.Interface()) {
				tmpVal, _ := strconv.ParseFloat(defaultVal, 64)
				valFiledTmp.SetFloat(tmpVal)
			}
		}
	}
}

// Load a src struct into a destination struct,structs must point,for diff struck type.
func Load(dst interface{}, src interface{}) {
	dstV := reflect.Indirect(reflect.ValueOf(dst))
	dstT := dstV.Type()
	srcV := reflect.Indirect(reflect.ValueOf(src))
	srcMap := make(map[string]reflect.Value)

	if !dstV.CanAddr() {
		z1err.Check(errors.New("copy to value is unaddressable"))
	}

	for i := 0; i < srcV.NumField(); i++ {
		fieldTop := srcV.Field(i)

		if fieldTop.Kind() == reflect.Struct {
			for j := 0; j < fieldTop.NumField(); j++ {
				fieldTmp := fieldTop.Field(j)
				if fieldTmp.Kind() != reflect.Struct {
					key := fmt.Sprintf("%s_%s", fieldTop.Type().Field(j).Name, fieldTop.Type().Field(j).Type)
					srcMap[key] = fieldTmp
				}
			}
		} else {
			fieldTmp := srcV.Field(i)
			if fieldTmp.Kind() != reflect.Struct {
				key := fmt.Sprintf("%s_%s", srcV.Type().Field(i).Name, srcV.Type().Field(i).Type)
				srcMap[key] = fieldTmp
			}
		}
	}

	for i := 0; i < dstV.NumField(); i++ {
		fieldTop := dstV.Field(i)

		if fieldTop.Kind() != reflect.Struct {
			key := fmt.Sprintf("%s_%s", dstT.Field(i).Name, dstT.Field(i).Type)
			if v, ok := srcMap[key]; ok {
				if !isZeroOfUnderlyingType(v.Interface()) {
					dstV.Field(i).Set(v)
				}
			}
		}
	}
}

// Copy a src struct into a destination struct,structs must point,for same struck type.
func Copy(dst interface{}, src interface{}) {
	dstV := reflect.Indirect(reflect.ValueOf(dst))
	srcV := reflect.Indirect(reflect.ValueOf(src))

	if !dstV.CanAddr() {
		z1err.Check(errors.New("copy to value is unaddressable"))
	}

	if srcV.Type() != dstV.Type() {
		z1err.Check(errors.New("different types can be copied"))
	}

	for i := 0; i < dstV.NumField(); i++ {
		f := srcV.Field(i)
		if !isZeroOfUnderlyingType(f.Interface()) {
			dstV.Field(i).Set(f)
		}
	}
}

// ------------util func--------------

func isZeroOfUnderlyingType(x interface{}) bool {
	return x == nil || reflect.DeepEqual(x, reflect.Zero(reflect.TypeOf(x)).Interface())
}
